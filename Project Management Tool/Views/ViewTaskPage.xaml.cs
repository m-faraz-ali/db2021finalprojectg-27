﻿using CommunityToolkit.WinUI.UI.Controls;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;
using Project_Management_Tool.DL;

namespace Project_Management_Tool.Views;

public sealed partial class ViewTaskPage : Page
{
    public ViewTaskViewModel ViewModel
    {
        get;
    }

    public ViewTaskPage()
    {
        ViewModel = App.GetService<ViewTaskViewModel>();
        InitializeComponent();

        var query = $"SELECT TaskName, TaskDescription, COALESCE(CONVERT(varchar(10), StartDate, 105), 'NULL') AS StartDate, COALESCE(CONVERT(varchar(10), EndDate, 105), 'NULL') AS EndDate, (SELECT Value FROM Lookup WHERE ID = TaskStatus) AS TaskStatus, (SELECT Value FROM Lookup WHERE ID = TaskPriority) AS TaskPriority FROM Task WHERE AssignedTo = {UserDL.CurrentUser.UserID};";

        Helper.FillColumns(query, dataGrid);

        query = $"SELECT TaskID, TaskName, TaskDescription, ProjectID, (Select ProjectName FROM Project WHERE ProjectID = ProjectID) AS ProjectName, COALESCE(CONVERT(varchar(10), StartDate, 105), 'NULL') AS StartDate, COALESCE(CONVERT(varchar(10), EndDate, 105), 'NULL') AS EndDate, (SELECT Value FROM Lookup WHERE ID = TaskStatus) AS TaskStatus, (SELECT Value FROM Lookup WHERE ID = TaskPriority) AS TaskPriority, AssignedTo AS AssignedEmployeeID, (SELECT CONCAT(FirstName, ' ', LastName) FROM Users WHERE UserID = AssignedTo) AS AssignedEmployeeName FROM Task  WHERE AssignedTo = {UserDL.CurrentUser.UserID};";


        Helper.FillDataGrid(query, dataGrid, "Task");
    }
}
