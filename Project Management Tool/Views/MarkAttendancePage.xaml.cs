﻿using System.Diagnostics;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Project_Management_Tool.DL;
using Project_Management_Tool.ViewModels;

namespace Project_Management_Tool.Views;

public sealed partial class MarkAttendancePage : Page
{
    public MarkAttendanceViewModel ViewModel
    {
        get;
    }

    public MarkAttendancePage()
    {
        ViewModel = App.GetService<MarkAttendanceViewModel>();
        InitializeComponent();

        var query = "Select UserID, CONCAT(FirstName, ' ', LastName) AS Name from Users where UserType = 5";
        Helper.FillComboBox(XamlRoot, query, ID_ComboBox, "EmployeeID");

        AttendanceDate_DatePicker.SelectedDate = DateTime.Now;
    }

    public void MarkAttendence_ButtonClick(object sender, RoutedEventArgs e)
    {
        if (ID_ComboBox.SelectedIndex == -1)
        {
            Helper.ShowErrorMessage(XamlRoot, "Please Select Employee");
            return;
        }

        var ID = ID_ComboBox.SelectedItem.ToString().Split("\t")[0];


        var AttendanceDate = AttendanceDate_DatePicker.Date.ToString();

        if (AttendanceStatus_ComboBox.SelectedIndex == -1)
        {
            Helper.ShowErrorMessage(XamlRoot, "Please Select Attendance Status");
            return;
        }

        var AttendanceStatus = AttendanceStatus_ComboBox.SelectedValue.ToString();
        
        

        int statusPresence;

        if (AttendanceStatus == "Present")
        {
            statusPresence = 6;
        }
        else
        {
            statusPresence = 7;
        }

        var query = $"Insert into Attendance (EmployeeID, AttendanceDate, AttendanceStatus, ManagerID) VALUES ({ID},'{AttendanceDate}',{statusPresence}, {UserDL.CurrentUser.UserID})";
        Helper.ExecuteQuery(XamlRoot, query);

        Helper.ShowMessageBox(XamlRoot, "Success", "Record Added Successfully");
    }
}
