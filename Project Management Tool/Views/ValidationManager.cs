﻿using Microsoft.UI.Xaml;
using System.Text.RegularExpressions;

namespace Project_Management_Tool.Views;
internal class ValidationManager
{
    public static bool ValidateFirstName(XamlRoot xamlRoot, string FirstName)
    {

        if (string.IsNullOrEmpty(FirstName))
        {
            Helper.ShowErrorMessage(xamlRoot, "First Name Cannot be Empty");
            return false;
        }
        
        if (FirstName.Length < 3)
        {
            Helper.ShowErrorMessage(xamlRoot, "First Name must be at least 3 characters long");
            return false;
        }

        if (FirstName.Length > 100)
        {
            Helper.ShowErrorMessage(xamlRoot, "First Name must be at most 100 characters long");
            return false;
        }

        var pattern = @"^[A-Z]([A-Za-z\s\.]+)?$";

        if (!Regex.IsMatch(FirstName, pattern))
        {
            Helper.ShowErrorMessage(xamlRoot, "First Name must start with a Capital letter and can only Contain letters, spaces and periods");
            return false;
        }

        return true;
    }

    public static bool ValidateLastName(XamlRoot xamlRoot, string LastName)
    {
        if (LastName.Length == 0) return true;

        if (LastName.Length < 3)
        {
            Helper.ShowErrorMessage(xamlRoot, "Last Name must be at least 3 characters long");
            return false;
        }

        if (LastName.Length > 100)
        {
            Helper.ShowErrorMessage(xamlRoot, "Last Name must be at most 100 characters long");
            return false;
        }

        var pattern = @"^[A-Z][a-z]+([\s][A-Z][a-z]+)?$";


        if (!Regex.IsMatch(LastName, pattern))
        {
            Helper.ShowErrorMessage(xamlRoot, "Last Name must start with a Capital letter and can only Contain letters, spaces and periods");
            return false;
        }

        return true;
    }

    public static bool ValidateContact(XamlRoot xamlRoot, string Contact)
    {
        if (Contact.Length == 0) return true;

        if (Contact.Length != 11)
        {
            Helper.ShowErrorMessage(xamlRoot, "Contact must be 11 characters long in the format 03XXXXXXXXX (X Represents Any Digit From 0 to 9)");
            return false;
        }

        var pattern = @"^[0-9]+$";

        if (!Regex.IsMatch(Contact, pattern))
        {
            Helper.ShowErrorMessage(xamlRoot, "Contact must only contain numbers");
            return false;
        }

        pattern = @"^03\d{9}$";

        if (!Regex.IsMatch(Contact, pattern))
        {
            Helper.ShowErrorMessage(xamlRoot, "Contact must start with 03 and can only contain 11 numbers");
            return false;
        }

        return true;
    }

    public static bool ValidateEmail(XamlRoot xamlRoot, string Email)
    {

        if (string.IsNullOrEmpty(Email))
        {
            Helper.ShowErrorMessage(xamlRoot, "Email Cannot be Empty");
            return false;
        }

        if (Email.Length < 5)
        {
            Helper.ShowErrorMessage(xamlRoot, "Email must be at least 5 characters long");
            return false;
        }

        if (Email.Length > 30)
        {
            Helper.ShowErrorMessage(xamlRoot, "Email must be at most 30 characters long");
            return false;
        }

        var pattern = @"^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]{2,}$";

        if (!Regex.IsMatch(Email, pattern))
        {
            Helper.ShowErrorMessage(xamlRoot, "Email must be in the Correct format");
            return false;
        }

        return true;
    }

    public static bool ValidatePassword(XamlRoot xamlRoot, string Password)
    {
        if (string.IsNullOrEmpty(Password))
        {
            Helper.ShowErrorMessage(xamlRoot, "Password Cannot be Empty");
            return false;
        }

        if (Password.Length < 8)
        {
            Helper.ShowErrorMessage(xamlRoot, "Password must be at least 8 characters long");
            return false;
        }

        if (Password.Length > 30)
        {
            Helper.ShowErrorMessage(xamlRoot, "Password must be at most 30 characters long");
            return false;
        }

        var pattern = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,30}$";

        if (!Regex.IsMatch(Password, pattern))
        {
            Helper.ShowErrorMessage(xamlRoot, "Password must contain at least 1 lowercase letter, 1 uppercase letter, 1 number and 1 special character");
            return false;
        }

        return true;
    }

    // Validate Username
    public static bool ValidateUsername(XamlRoot xamlRoot, string Username)
    {
        if (string.IsNullOrEmpty(Username))
        {
            Helper.ShowErrorMessage(xamlRoot, "Username Cannot be Empty");
            return false;
        }

        if (Username.Length < 3)
        {
            Helper.ShowErrorMessage(xamlRoot, "Username must be at least 3 characters long");
            return false;
        }

        if (Username.Length > 30)
        {
            Helper.ShowErrorMessage(xamlRoot, "Username must be at most 30 characters long");
            return false;
        }

        var pattern = @"^[a-zA-Z0-9]+$";

        if (!Regex.IsMatch(Username, pattern))
        {
            Helper.ShowErrorMessage(xamlRoot, "Username must only contain letters and numbers");
            return false;
        }

        return true;
    }

    public static bool ValidateInteger(XamlRoot xamlRoot, string Integer, string IntegerName)
    {

        if (string.IsNullOrEmpty(Integer))
        {
            Helper.ShowErrorMessage(xamlRoot, $"{IntegerName} Cannot be Empty");
            return false;
        }

        var pattern = @"^\d+$";

        if (!Regex.IsMatch(Integer, pattern))
        {
            Helper.ShowErrorMessage(xamlRoot, $"{IntegerName} is not an integer");
            return false;
        }

        return true;
    }

    
}


