﻿using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;
using Project_Management_Tool.BL;


namespace Project_Management_Tool.Views;

public sealed partial class UpdateProjectPage : Page
{
    DateTimeOffset d1;
    DateTimeOffset d2;
    public UpdateProjectViewModel ViewModel
    {
        get;
    }

    public UpdateProjectPage()
    {
        ViewModel = App.GetService<UpdateProjectViewModel>();
        InitializeComponent();
        Helper.FillComboBoxProjectID(XamlRoot, ProjectID_ComboBox);
        d1 = ProjectStart_Date.Date;
        d2 = ProjectEnd_Date.Date;
    }
    public void UpdateProject_ButonClicked(object sender, RoutedEventArgs e)
    {
        var id = ProjectID_ComboBox.SelectedItem.ToString().Split("\t")[0];
        var name = ProjectName_TextBox.Text;
        var description = ProjectDescription_TextBox.Text;
        var startDate = ProjectStart_Date.Date;
        var endDate = ProjectEnd_Date.Date;

        if (id == null || name == "" || startDate == d1 || endDate == d2 || description == "")
        {
            Helper.ShowErrorMessage(XamlRoot, "Please fill all the fields");
            return;
        }
        if (startDate > endDate)
        {
            Helper.ShowErrorMessage(XamlRoot, "Start Date cannot be greater than End Date");
            return;
        }
        var newProj = new Project()
        {
            ProjectName = name,
            StartDate = startDate.ToString(),
            EndDate = endDate.ToString(),
            ProjectDescription = description
        };
        try
        {
            DL.UpdateProjectById(id, newProj);
        }
        catch (Exception error)
        {
            Helper.ShowErrorMessage(XamlRoot, error.Message);
            return;
        }
        Helper.ShowMessageBox(XamlRoot, "Success!", "Project Updated Successfully!");
    }
}
