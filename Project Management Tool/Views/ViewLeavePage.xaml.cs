﻿using CommunityToolkit.WinUI.UI.Controls;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;
using Project_Management_Tool.DL;

namespace Project_Management_Tool.Views;

public sealed partial class ViewLeavePage : Page
{
    public ViewLeaveViewModel ViewModel
    {
        get;
    }

    public ViewLeavePage()
    {
        ViewModel = App.GetService<ViewLeaveViewModel>();
        InitializeComponent();

        var query = $"SELECT COALESCE(CONVERT(varchar(10), StartDate, 105), 'NULL') AS StartDate, COALESCE(CONVERT(varchar(10), EndDate, 105), 'NULL') AS EndDate, (SELECT Value FROM Lookup WHERE ID = LeaveType) AS LeaveType, (SELECT Value FROM Lookup WHERE ID = LeaveStatus) AS LeaveStatus FROM LeaveRequest WHERE EmployeeID = {UserDL.CurrentUser.UserID}";
        Helper.FillColumns(query, dataGrid);
        query = $"SELECT LeaveID, EmployeeID, (SELECT CONCAT(FirstName, ' ', LastName) FROM Users WHERE UserID = EmployeeID) AS EmployeeName, COALESCE(CONVERT(varchar(10), StartDate, 105), 'NULL') AS StartDate, COALESCE(CONVERT(varchar(10), EndDate, 105), 'NULL') AS EndDate, (SELECT Value FROM Lookup WHERE ID = LeaveType) AS LeaveType, (SELECT Value FROM Lookup WHERE ID = LeaveStatus) AS LeaveStatus FROM LeaveRequest WHERE EmployeeID = {UserDL.CurrentUser.UserID}";
        
        Helper.FillDataGrid(query, dataGrid, "LeaveRequest");

    }
}
