﻿using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;

namespace Project_Management_Tool.Views;

public sealed partial class AddMembersPage : Page
{
    public AddMembersViewModel ViewModel
    {
        get;
    }

    public AddMembersPage()
    {
        ViewModel = App.GetService<AddMembersViewModel>();
        InitializeComponent();

        Helper.FillComboBoxProjectID(XamlRoot, ProjectTeamID_ComboBox);
        string query = "select id, Value from Lookup where Category = 'Role'";
        Helper.FillComboBox(XamlRoot, query, Role_ComboBox, "Role");
    }
    public void AddMember_ButtonClick(object sender, RoutedEventArgs e)
    {
        string? id = ProjectTeamID_ComboBox.SelectedItem.ToString().Split("\t")[0];
        string? emp = Employee_ComboBox.SelectedItem.ToString().Split("\t")[0];
        string role = Role_ComboBox.SelectedItem.ToString().Split("\t")[0];
        string? query = $"insert into ProjectTeamMember values((select ProjectTeamID from ProjectTeam where ProjectID = {id}), {emp}, {role})";
        try
        {
            Helper.ExecuteQuery(XamlRoot, query);
        }
        catch (Exception error)
        {
            Helper.ShowErrorMessage(XamlRoot, error.Message);
            return;
        }
        Helper.ShowMessageBox(XamlRoot, "Success!", "Member Added Successfuly!");
    }

    private void ProjectTeamID_ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        int id = int.Parse(ProjectTeamID_ComboBox.SelectedItem.ToString().Split("\t")[0]);
        string query = $"select UserID, CONCAT(FirstName, ' ', LastName) AS FullName from Users where UserID not in(select PTM.EmployeeID from ProjectTeamMember as PTM inner join ProjectTeam as PT on PT.ProjectTeamID = PTM.ProjectTeamID where PT.ProjectID = {id})";
        Helper.FillComboBox(XamlRoot, query, Employee_ComboBox, "AddMember");
    }
}
