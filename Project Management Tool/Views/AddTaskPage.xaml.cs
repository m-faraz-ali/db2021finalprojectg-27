﻿using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;

namespace Project_Management_Tool.Views;

public sealed partial class AddTaskPage : Page
{
    public AddTaskViewModel ViewModel
    {
        get;
    }
    DateTimeOffset d1, d2;
    public AddTaskPage()
    {
        ViewModel = App.GetService<AddTaskViewModel>();
        InitializeComponent();
        Helper.FillComboBoxProjectID(XamlRoot, ProjectID_ComboBox);
        string query = "select id, Value from Lookup where Category = 'TaskStatus'";
        Helper.FillComboBox(XamlRoot, query, TaskStatus_ComboBox, "TaskStatus");
        query = "select id, Value from Lookup where Category = 'TaskPriority'";
        Helper.FillComboBox(XamlRoot, query, TaskPriority_ComboBox, "TaskPriority");
        d1 = StartDate_DatePicker.Date;
        d2 = EndDate_DatePicker.Date;
    }

    public void AddTask_ButtonClick(object sender, RoutedEventArgs e)
    {
        var id = ProjectID_ComboBox.SelectedValue.ToString().Split("\t")[0];
        var name = TaskName_TextBox.Text.ToString();
        var desc = TaskDescription_TextBox.Text.ToString();
        DateTimeOffset start = StartDate_DatePicker.Date;
        DateTimeOffset end = EndDate_DatePicker.Date;
        var status = TaskStatus_ComboBox.SelectedValue.ToString().Split("\t")[0];
        var priority = TaskPriority_ComboBox.SelectedValue.ToString().Split("\t")[0];
        var assignedTo = AssignedTo_ComboBox.SelectedValue.ToString().Split("\t")[0];

        if (id == "" || name == "" || desc == "" || start == d1 || end == d2 || status == null || priority == null || assignedTo == null)
        {
            Helper.ShowErrorMessage(XamlRoot, "Input Error!");
        }
        else if (start > end)
        {
            Helper.ShowErrorMessage(XamlRoot, "Invalid Date!");
        }
        else
        {
            string query = $"insert into Task values ('{id}', '{name}', '{desc}', '{start.Date.ToString()}', '{end.Date.ToString()}', {status}, {priority}, {assignedTo})";
            try
            {
                Helper.ExecuteQuery(query);
            }
            catch (Exception error)
            {
                Helper.ShowErrorMessage(XamlRoot, error.Message + "\\999");
                return;
            }
            Helper.ShowMessageBox(XamlRoot, "Success", "Task added Successfully!");
        }
    }

    private void ProjectID_ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        var id = ProjectID_ComboBox.SelectedValue.ToString().Split("\t")[0];
        string query = $"select UserID , CONCAT(FirstName, ' ', LastName) as FullName from Users where UserID in (select PTM.EmployeeID from ProjectTeamMember as PTM inner join ProjectTeam as PM on PTM.ProjectTeamID = PM.ProjectTeamID where PM.ProjectID = '{id}')";
        Helper.FillComboBox(XamlRoot, query, AssignedTo_ComboBox, "AddMember");
    }
}
