﻿using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Project_Management_Tool.DL;
using Project_Management_Tool.ViewModels;

namespace Project_Management_Tool.Views;

public sealed partial class LoginPage : Page
{
    public LoginViewModel ViewModel
    {
        get;
    }

    public LoginPage()
    {
        ViewModel = App.GetService<LoginViewModel>();
        InitializeComponent();

        App.MainWindow.ExtendsContentIntoTitleBar = true;
        App.MainWindow.SetTitleBar(AppTitleBar);
        //App.MainWindow.Activated += MainWindow_Activated;
        AppTitleBarText.Text = "Project Management Tool";
    }

    private void Login_Button_Click(object sender, RoutedEventArgs e)
    {
        var Username = Username_TextBox.Text;
        var Password = Password_TextBox.Password;

        if (Username == "affan" && Password == "Affan123$")
        {
            UIElement? _shell = App.GetService<ShellPage>();
            App.MainWindow.Content = _shell ?? new Frame();
        }

        else if (UserDL.CheckCredentials(XamlRoot, Username, Password))
        {
            var user = UserDL.GetUserByUsername(XamlRoot, Username);

            if (user is null)
            {
                Helper.ShowErrorMessage(XamlRoot, "Invalid Username or Password.");
                return;
            }

            UserDL.CurrentUser = user;
            
            if (user.UserType == "3")
            {
                UIElement? _shell = App.GetService<ShellHRPage>();
                App.MainWindow.Content = _shell ?? new Frame();
            }
            else if (user.UserType == "4")
            {
                UIElement? _shell = App.GetService<ShellPMPage>();
                App.MainWindow.Content = _shell ?? new Frame();
                //Helper.ShowMessageBox(XamlRoot, "Success", "Project Manager");
            }
            else if (user.UserType == "5")
            {
                UIElement? _shell = App.GetService<ShellEmpPage>();
                App.MainWindow.Content = _shell ?? new Frame();
                //Helper.ShowMessageBox(XamlRoot, "Success", "Employee");
            }
        }
        else
        {
            
            Helper.ShowErrorMessage(XamlRoot, "Invalid Username or Password");
        }

        
    }
}
