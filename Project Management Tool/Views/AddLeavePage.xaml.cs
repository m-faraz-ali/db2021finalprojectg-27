﻿using System.Diagnostics;
using Microsoft.UI.Xaml.Controls;
using Project_Management_Tool.Models;

using Project_Management_Tool.ViewModels;
using Windows.ApplicationModel.Activation;
using Project_Management_Tool.DL;
using Project_Management_Tool.BL;

#pragma warning disable CS8600

namespace Project_Management_Tool.Views;

public sealed partial class AddLeavePage : Page
{
    public AddLeaveViewModel ViewModel
    {
        get;
    }

    public AddLeavePage()
    {
        ViewModel = App.GetService<AddLeaveViewModel>();
        InitializeComponent();
        //var query = "SELECT UserID , CONCAT(Firstname , ' ' , LastName) as FullName FROM Users WHERE UserType = 3";
        //Helper.FillComboBox(XamlRoot, query, ManagerEmail_ComboBox, "ManagerEmail");
    }

    private void AddLeave_Button_Click(object sender, Microsoft.UI.Xaml.RoutedEventArgs e)
    {
        var startDate = StartDate_DatePicker.Date;
        var endDate = EndDate_DatePicker.Date;
        //var managerID = ManagerEmail_ComboBox.SelectedItem.ToString().Split("\t")[0];
        
        if(LeaveType_ComboBox.SelectedIndex < 0)
        {
            Helper.ShowErrorMessage(XamlRoot, "Please select a leave type");
            return;
        }
        
        string leave = LeaveType_ComboBox.SelectedItem.ToString();
        string leaveType;

        if (startDate > endDate)
        {
            Helper.ShowMessageBox(XamlRoot, "Error", "Start Date cannot be greater than End Date");
            return;
        }

        //else if (managerID == "" || leave == "")
        //{

        //    Helper.ShowMessageBox(XamlRoot, "Error", "Please fill all the fields");
        //    return;
        //}

        
        if (leave == "Sick Leave")
        {
            leaveType = "8";
        }
        else
        {
            leaveType = "9";
        }

        string leaveStatus = "10";

        LeaveRequest leaveRequest = new LeaveRequest(UserDL.CurrentUser.UserID, startDate.ToString(), endDate.ToString(), leaveType, leaveStatus);
        LeaveRequestDL.AddLeaveRequestToDB(XamlRoot, leaveRequest);



        Helper.ShowMessageBox(XamlRoot , "Success" , "Request Submitted");

    }
}
