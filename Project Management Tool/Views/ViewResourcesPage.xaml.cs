﻿using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;

namespace Project_Management_Tool.Views;

public sealed partial class ViewResourcesPage : Page
{
    public ViewResourcesViewModel ViewModel
    {
        get;
    }

    public ViewResourcesPage()
    {
        ViewModel = App.GetService<ViewResourcesViewModel>();
        InitializeComponent();

        var query = "SELECT TaskID, (SELECT TaskName FROM Task WHERE TaskID = TR.TaskID) AS TaskName, R.ResourceID, ResourceName, (SELECT Value FROM Lookup WHERE ID = ResourceType) AS ResourceType, Cost AS ResourceCost FROM Resource R JOIN TaskResource TR ON R.ResourceID = TR.ResourceID";
        Helper.FillColumns(query, dataGrid);
        Helper.FillDataGrid(query, dataGrid, "TaskResource");
    }
}
