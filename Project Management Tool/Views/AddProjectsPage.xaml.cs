﻿using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;
using Project_Management_Tool.BL;
using Project_Management_Tool.DL;

namespace Project_Management_Tool.Views;

public sealed partial class AddProjectsPage : Page
{
    DateTimeOffset OldDate1;
    DateTimeOffset OldDate2;
    
    public AddProjectsViewModel ViewModel
    {
        get;
    }

    public AddProjectsPage()
    {
        ViewModel = App.GetService<AddProjectsViewModel>();
        InitializeComponent();

        OldDate1 = ProjectStart_Date.Date;
        OldDate2 = ProjectEnd_Date.Date;

        string query = "select * from Lookup where Category = 'ResourceType'";
        //Helper.FillComboBox(XamlRoot, query,ResourceType_ComboBox, "ResourseType"); 
    }

    public void AddProject_ButtonClick(object sender, RoutedEventArgs e)
    {
        string name = ProjectName_TextBox.Text;
        string description = ProjectDescription_TextBox.Text;
        DateTimeOffset startTime = ProjectStart_Date.Date;
        DateTimeOffset endTime = ProjectEnd_Date.Date;

        if (name == "" || startTime == OldDate1 || endTime == OldDate2 || description == "")
        {
            Helper.ShowErrorMessage(XamlRoot, "Incomplete Data Error!");
        }
        else if (startTime > endTime)
        {
            Helper.ShowErrorMessage(XamlRoot, "Invalid Date Error!");
        }
        else
        {
            Project p = new Project();
            
            if (UserDL.CurrentUser is null) return;

            p.ManagerID = UserDL.CurrentUser.UserID;
            p.ProjectName = name;
            p.ProjectDescription = description;
            p.StartDate = startTime.ToString();
            p.EndDate = endTime.ToString();
            try
            {
                DL.AddProject(p);
            }
            catch (Exception ex)
            {
                Helper.ShowErrorMessage(XamlRoot, ex.Message);
                return;
            }
            Helper.ShowMessageBox(XamlRoot, "Success!", "Project Added Successfully!");
        }

    }
}
