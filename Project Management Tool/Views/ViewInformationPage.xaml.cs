﻿using CommunityToolkit.WinUI.UI.Controls;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;
using Project_Management_Tool.DL;

namespace Project_Management_Tool.Views;

public sealed partial class ViewInformationPage : Page
{
    public ViewInformationViewModel ViewModel
    {
        get;
    }

    public ViewInformationPage()
    {
        ViewModel = App.GetService<ViewInformationViewModel>();
        InitializeComponent();

       
        var query = $"Select FirstName, COALESCE(LastName, 'NULL') AS LastName, COALESCE(CONVERT(varchar(10), DateOfBirth, 105), 'NULL') AS DateOfBirth, COALESCE((SELECT Value FROM Lookup WHERE ID = Gender), 'NULL') AS Gender, Email, COALESCE(PhoneNumber, 'NULL') AS PhoneNumber FROM Users WHERE UserID = {UserDL.CurrentUser.UserID}";
        Helper.FillColumns(query, dataGrid);
        var query2 = $"Select UserID, FirstName, COALESCE(LastName, 'NULL') AS LastName, COALESCE(CONVERT(varchar(10), DateOfBirth, 105), 'NULL') AS DateOfBirth, COALESCE((SELECT Value FROM Lookup WHERE ID = Gender), 'NULL') AS Gender, Email, COALESCE(PhoneNumber, 'NULL') AS PhoneNumber FROM Users WHERE UserID = {UserDL.CurrentUser.UserID}";
        Helper.FillDataGrid(query2, dataGrid , "User");

    }
}
