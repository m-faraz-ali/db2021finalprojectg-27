﻿using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;

using Project_Management_Tool.BL;

namespace Project_Management_Tool.Views;

public sealed partial class ViewHRManagersPage : Page
{
    public bool isCredentials = false;
    
    public ViewHRManagersViewModel ViewModel
    {
        get;
    }

    public ViewHRManagersPage()
    {
        ViewModel = App.GetService<ViewHRManagersViewModel>();
        InitializeComponent();

        Refresh();
    }

    private void CredentialsButton_Click(object sender, RoutedEventArgs e)
    {
        isCredentials = !isCredentials;

        if (isCredentials)
        {
            var query = "Select U.UserID, FirstName, COALESCE(LastName, 'NULL') AS LastName, COALESCE(CONVERT(varchar(10), DateOfBirth, 105), 'NULL') AS DateOfBirth, COALESCE((SELECT Value FROM Lookup WHERE ID = Gender), 'NULL') AS Gender, Email, COALESCE(PhoneNumber, 'NULL') AS PhoneNumber, Username, Password FROM Users U JOIN [Credentials] C ON U.UserID = C.UserID WHERE U.UserType = 3; ";

            Helper.FillColumns(query, dataGrid);

            Helper.FillDataGrid(query, dataGrid, "UserWithCredentials");

        }
        else
        {
            Refresh();
        }


    }
    private void RefreshButton_Click(object sender, RoutedEventArgs e)
    {
        Refresh();
    }

    private void Refresh()
    {
        var query = "Select UserID, FirstName, COALESCE(LastName, 'NULL') AS LastName, COALESCE(CONVERT(varchar(10), DateOfBirth, 105), 'NULL') AS DateOfBirth, COALESCE((SELECT Value FROM Lookup WHERE ID = Gender), 'NULL') AS Gender, Email, COALESCE(PhoneNumber, 'NULL') AS PhoneNumber FROM Users WHERE UserType = 3";

        Helper.FillColumns(query, dataGrid);

        Helper.FillDataGrid(query, dataGrid, "User");
    }

    private void UpdateButton_Click(object sender, RoutedEventArgs e)
    {
        var selectedIndex = dataGrid.SelectedIndex;

        if (selectedIndex == -1)
        {
            Helper.ShowErrorMessage(XamlRoot, "No Row is Selected! Please Select a Row to Edit it.");
            return;
        }

        var selectedItem = dataGrid.SelectedItem as User;

        Frame.Navigate(typeof(UpdateHRManagerPage), selectedItem);
    }
}
