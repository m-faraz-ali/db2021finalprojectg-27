﻿using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;

namespace Project_Management_Tool.Views;

public sealed partial class AttendanceHistoryPage : Page
{
    public AttendanceHistoryViewModel ViewModel
    {
        get;
    }

    public AttendanceHistoryPage()
    {
        ViewModel = App.GetService<AttendanceHistoryViewModel>();
        InitializeComponent();

        var query = "SELECT EmployeeID, (SELECT CONCAT(FirstName, ' ', LastName) FROM Users WHERE UserID = EmployeeID) AS EmployeeName, CONVERT(varchar(10), AttendanceDate, 105) AS AttendanceDate, (SELECT Value FROM Lookup WHERE ID = AttendanceStatus) AS AttendanceStatus FROM Attendance;";

        Helper.FillColumns(query, dataGrid);

        Helper.FillDataGrid(query, dataGrid, "Attendance");
    }
}
