﻿using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;

namespace Project_Management_Tool.Views;

public sealed partial class ViewProjectsPage : Page
{
    public ViewProjectsViewModel ViewModel
    {
        get;
    }

    public ViewProjectsPage()
    {
        ViewModel = App.GetService<ViewProjectsViewModel>();
        InitializeComponent();

        var query = "SELECT ProjectID, ProjectName, COALESCE(CONVERT(varchar(10), StartDate, 105), 'NULL') AS StartDate, COALESCE(CONVERT(varchar(10), EndDate, 105), 'NULL') AS EndDate, (SELECT Value FROM Lookup WHERE ID = ProjectStatus) AS ProjectStatus, ManagerID, (SELECT CONCAT(FirstName, ' ', LastName) FROM Users WHERE UserID = ManagerID) AS ManagerName, COALESCE(ProjectDescription, 'NULL') AS ProjectDescription FROM Project";
        Helper.FillColumns(query, dataGrid);
        Helper.FillDataGrid(query, dataGrid, "Project");
    }
}
