﻿using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;

namespace Project_Management_Tool.Views;

public sealed partial class UpdateResourcePage : Page
{
    public UpdateResourceViewModel ViewModel
    {
        get;
    }

    public UpdateResourcePage()
    {
        ViewModel = App.GetService<UpdateResourceViewModel>();
        InitializeComponent();
        Helper.FillComboBoxProjectID(XamlRoot, ProjectID_ComboBox);
        string query = "select ID, Value from Lookup where Category = 'ResourceType'";
        Helper.FillComboBox(XamlRoot, query, ResourceType_ComboBox, "ResourceType");
    }

    private void ProjectID_ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        var projId = ProjectID_ComboBox.SelectedItem.ToString().Split("\t")[0];
        string query = $"select TaskID, TaskName from Task where ProjectID = {projId}";
        Helper.FillComboBox(XamlRoot, query, TaskID_ComboBox, "TaskID");
    }

    private void UpdateResource_ButtonClick(object sender, Microsoft.UI.Xaml.RoutedEventArgs e)
    {
        string resourceID = ResourceName_ComboBox.SelectedItem.ToString().Split("\t")[0];
        string resourceType = ResourceType_ComboBox.SelectedItem.ToString().Split("\t")[0];
        string cost = Cost_TextBox.Text;


        if (resourceID == null || resourceType == null || cost == null)
        {
            Helper.ShowErrorMessage(XamlRoot, "Incomplete Input!");
            return;
        }


        string query = $"update Resource set ResourceType = {resourceType}, Cost = {cost} where ResourceID = {resourceID}";
        try
        {
            Helper.ExecuteQuery(query);
        }
        catch (Exception error)
        {
            Helper.ShowErrorMessage(XamlRoot, error.Message);
            return;
        }
        Helper.ShowMessageBox(XamlRoot, "Success!", "Resource Updated Successfully!");
    }

    private void TaskID_ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        string taskID = TaskID_ComboBox.SelectedItem.ToString().Split("\t")[0];
        string query = "select R.ResourceID, R.ResourceName from Resource R inner join TaskResource TR on R.ResourceID = TR.ResourceID where TR.TaskID = " + taskID;
        Helper.FillComboBox(XamlRoot, query, ResourceName_ComboBox, "ResourceName");
    }
}
