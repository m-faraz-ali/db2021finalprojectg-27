﻿using System.Data.SqlClient;
using System.Data;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml;
using CommunityToolkit.WinUI.UI.Controls;
using Microsoft.UI.Xaml.Data;
//using CommunityToolkit.Common.Collections;
using Project_Management_Tool.BL;
using Project_Management_Tool.DL;

namespace Project_Management_Tool.Views;

#pragma warning disable CS8602
#pragma warning disable CS8603
#pragma warning disable CS8604
#pragma warning disable IDE0019
#pragma warning disable CS0659
#pragma warning disable CS8765


public class Helper
{
    public static void FillComboBox(XamlRoot xamlRoot, string query, ComboBox comboBox, string columnName)
    {
        var con = Configuration.getInstance().getConnection();

        var cmd = new SqlCommand(query, con);

        try
        {
            var adapter = new SqlDataAdapter(cmd);

            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            foreach (DataRow row in dataTable.Rows)
            {
                try
                {
                    if (columnName == "Gender")
                    {
                        comboBox.Items.Add(row["Value"]);
                    }
                    else if (columnName == "EmployeeID")
                    {
                        comboBox.Items.Add(row["UserID"] + "\t" + row["Name"]);
                    }

                    else if (columnName == "AddMember")
                    {
                        comboBox.Items.Add(row["UserID"] + "\t" + row["FullName"]);
                    }
                    else if (columnName == "Role")
                    {
                        comboBox.Items.Add(row["ID"] + "\t" + row["Value"]);
                    }
                    else if (columnName == "TaskStatus")
                    {
                        comboBox.Items.Add(row["ID"] + "\t" + row["Value"]);
                    }
                    else if (columnName == "TaskPriority")
                    {
                        comboBox.Items.Add(row["id"] + "\t" + row["Value"]);
                    }
                    else if (columnName == "TaskID")
                    {
                        comboBox.Items.Add(row["TaskID"] + "\t" + row["TaskName"]);
                    }
                    else if (columnName == "ResourceType")
                    {
                        comboBox.Items.Add(row["ID"] + "\t" + row["Value"]);
                    }
                    else if (columnName == "ResourceName")
                    {
                        
                        comboBox.Items.Add(row["ResourceID"] + "\t" + row["ResourceName"]);
                    }

                }
                catch (Exception error)
                {
                    ShowMessageBox(xamlRoot, "Error", error.Message);
                    return;
                }
            }
        }
        catch (Exception error)
        {
            ShowMessageBox(xamlRoot, "Error", error.Message);
            return;
        }
    }

    public static async void ShowMessageBox(XamlRoot xamlRoot, string title, string content)
    {
        var dialog = new ContentDialog()
        {
            XamlRoot = xamlRoot,
            Title = title,
            Content = content,
            PrimaryButtonText = "Ok",
            DefaultButton = ContentDialogButton.Primary
        };

        await dialog.ShowAsync();
    }

    public static async void ShowErrorMessage(XamlRoot xamlRoot, string message)
    {
        ContentDialog dialog = new ContentDialog();

        dialog.XamlRoot = xamlRoot;
        dialog.Style = Application.Current.Resources["DefaultContentDialogStyle"] as Style;
        dialog.Title = "Error";
        dialog.PrimaryButtonText = "Discard";
        dialog.DefaultButton = ContentDialogButton.Primary;
        dialog.Content = message;

        await dialog.ShowAsync();
        return;
    }

    public static async Task<int> ShowConfirmationDialog(XamlRoot xamlRoot, string message)
    {
        ContentDialog dialog = new ContentDialog();

        dialog.XamlRoot = xamlRoot;
        dialog.Style = Application.Current.Resources["DefaultContentDialogStyle"] as Style;
        dialog.Title = "Confirmation";
        dialog.PrimaryButtonText = "Yes";
        dialog.SecondaryButtonText = "No";
        dialog.DefaultButton = ContentDialogButton.Primary;
        dialog.Content = message;

        var result = await dialog.ShowAsync();

        if (result == ContentDialogResult.Primary)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public static void FillDataGrid(string query, DataGrid dataGrid, string TableName)
    {
        var con = Configuration.getInstance().getConnection();

        SqlCommand command = new SqlCommand(query, con);

        SqlDataAdapter adapter = new SqlDataAdapter(command);

        DataTable dataTable = new DataTable();

        adapter.Fill(dataTable);

        if (TableName == "User")
        {
            var itemsArray = new User[dataTable.Rows.Count];

            for (var i = 0; i < dataTable.Rows.Count; i++)
            {
                var ID = dataTable.Rows[i][0].ToString();
                var FirstName = dataTable.Rows[i][1].ToString();
                var LastName = dataTable.Rows[i][2].ToString();
                var DateOfBirth = dataTable.Rows[i][3].ToString();
                var Gender = dataTable.Rows[i][4].ToString();
                var Email = dataTable.Rows[i][5].ToString();
                var PhoneNumber = dataTable.Rows[i][6].ToString();

                var x = new User(ID, FirstName, LastName, DateOfBirth, Gender, Email, PhoneNumber);
                itemsArray[i] = x;
            }

            dataGrid.ItemsSource = itemsArray;
        }
        else if (TableName == "UserWithCredentials")
        {
            var itemsArray = new User[dataTable.Rows.Count];

            for (var i = 0; i < dataTable.Rows.Count; i++)
            {
                var ID = dataTable.Rows[i][0].ToString();
                var FirstName = dataTable.Rows[i][1].ToString();
                var LastName = dataTable.Rows[i][2].ToString();
                var DateOfBirth = dataTable.Rows[i][3].ToString();
                var Gender = dataTable.Rows[i][4].ToString();
                var Email = dataTable.Rows[i][5].ToString();
                var PhoneNumber = dataTable.Rows[i][6].ToString();
                var Username = dataTable.Rows[i][7].ToString();
                var Password = dataTable.Rows[i][8].ToString();


                var x = new User(ID, FirstName, LastName, DateOfBirth, Gender, Email, PhoneNumber, Username, Password);
                itemsArray[i] = x;
            }

            dataGrid.ItemsSource = itemsArray;
        }

        else if (TableName == "LeaveRequest")
        {
            var itemsArray = new LeaveRequest[dataTable.Rows.Count];

            for (var i = 0; i < dataTable.Rows.Count; i++)
            {
                var LeaveID = dataTable.Rows[i][0].ToString();
                var EmployeeID = dataTable.Rows[i][1].ToString();
                var EmployeeName = dataTable.Rows[i][2].ToString();
                var StartDate = dataTable.Rows[i][3].ToString();
                var EndDate = dataTable.Rows[i][4].ToString();
                var LeaveType = dataTable.Rows[i][5].ToString();
                var LeaveStatus = dataTable.Rows[i][6].ToString();

                var x = new LeaveRequest(LeaveID, EmployeeID, EmployeeName, StartDate, EndDate, LeaveType, LeaveStatus);
                itemsArray[i] = x;
            }

            dataGrid.ItemsSource = itemsArray;
        }
        else if (TableName == "Project")
        {
            var itemsArray = new Project[dataTable.Rows.Count];

            for (var i = 0; i < dataTable.Rows.Count; i++)
            {
                var ProjectID = dataTable.Rows[i]["ProjectID"].ToString();
                var ProjectName = dataTable.Rows[i]["ProjectName"].ToString();
                var ProjectDescription = dataTable.Rows[i]["ProjectDescription"].ToString();
                var StartDate = dataTable.Rows[i]["StartDate"].ToString();
                var EndDate = dataTable.Rows[i]["EndDate"].ToString();
                var ProjectStatus = dataTable.Rows[i]["ProjectStatus"].ToString();
                var ManagerID = dataTable.Rows[i]["ManagerID"].ToString();
                var ManagerName = dataTable.Rows[i]["ManagerName"].ToString();

                var p = new Project(ProjectID, ProjectName, ProjectDescription, StartDate, EndDate, ProjectStatus, ManagerID, ManagerName);
                itemsArray[i] = p;

            }

            dataGrid.ItemsSource = itemsArray;
        }

        else if (TableName == "ProjectTeamMember")
        {
            var itemsArray = new ProjectTeamMember[dataTable.Rows.Count];

            for (var i = 0; i < dataTable.Rows.Count; i++)
            {
                var ProjectTeamID = dataTable.Rows[i]["ProjectTeamID"].ToString();
                var ProjectID = dataTable.Rows[i]["ProjectID"].ToString();
                var ProjectName = dataTable.Rows[i]["ProjectName"].ToString();
                var EmployeeID = dataTable.Rows[i]["EmployeeID"].ToString();
                var EmployeeName = dataTable.Rows[i]["EmployeeName"].ToString();
                var EmployeeRole = dataTable.Rows[i]["EmployeeRole"].ToString();
                var AssignmentDate = dataTable.Rows[i]["AssignmentDate"].ToString();

                var ptm = new ProjectTeamMember(ProjectTeamID, ProjectID, ProjectName, EmployeeID, EmployeeName, EmployeeRole, AssignmentDate);

                itemsArray[i] = ptm;
            }

            dataGrid.ItemsSource = itemsArray;
        }

        else if (TableName == "Task")
        {
            var itemsArray = new BL.Task[dataTable.Rows.Count];

            for (var i = 0; i < dataTable.Rows.Count; i++)
            {
                var TaskID = dataTable.Rows[i]["TaskID"].ToString();
                var TaskName = dataTable.Rows[i]["TaskName"].ToString();
                var TaskDescription = dataTable.Rows[i]["TaskDescription"].ToString();
                var ProjectID = dataTable.Rows[i]["ProjectID"].ToString();
                var ProjectName = dataTable.Rows[i]["ProjectName"].ToString();
                var StartDate = dataTable.Rows[i]["StartDate"].ToString();
                var EndDate = dataTable.Rows[i]["EndDate"].ToString();
                var TaskStatus = dataTable.Rows[i]["TaskStatus"].ToString();
                var TaskPriority = dataTable.Rows[i]["TaskPriority"].ToString();
                var AssignedEmployeeID = dataTable.Rows[i]["AssignedEmployeeID"].ToString();
                var AssignedEmployeeName = dataTable.Rows[i]["AssignedEmployeeName"].ToString();

                var task = new BL.Task(TaskID, TaskName, TaskDescription, ProjectID, ProjectName, StartDate, EndDate, TaskStatus, TaskPriority, AssignedEmployeeID, AssignedEmployeeName);

                itemsArray[i] = task;
            }

            dataGrid.ItemsSource = itemsArray;
        }

        else if (TableName == "TaskResource")
        {
            var itemsArray = new TaskResource[dataTable.Rows.Count];

            for (var i = 0; i < dataTable.Rows.Count; i++)
            {
                var TaskID = dataTable.Rows[i]["TaskID"].ToString();
                var TaskName = dataTable.Rows[i]["TaskName"].ToString();
                var ResourceID = dataTable.Rows[i]["ResourceID"].ToString();
                var ResourceName = dataTable.Rows[i]["ResourceName"].ToString();
                var ResourceType = dataTable.Rows[i]["ResourceType"].ToString();
                var ResourceCost = dataTable.Rows[i]["ResourceCost"].ToString();


                var task = new TaskResource(TaskID, TaskName, ResourceID, ResourceName, ResourceType, ResourceCost);

                itemsArray[i] = task;
            }

            dataGrid.ItemsSource = itemsArray;
        }

        else if (TableName == "Attendance")
        {
            var itemsArray = new Attendance[dataTable.Rows.Count];

            for (var i = 0; i < dataTable.Rows.Count; i++)
            {
                var EmployeeID = dataTable.Rows[i]["EmployeeID"].ToString();
                var EmployeeName = dataTable.Rows[i]["EmployeeName"].ToString();
                var AttendanceDate = dataTable.Rows[i]["AttendanceDate"].ToString();
                var AttendanceStatus = dataTable.Rows[i]["AttendanceStatus"].ToString();

                var record = new Attendance(EmployeeID, EmployeeName, AttendanceDate, AttendanceStatus);
                
                itemsArray[i] = record;
            }

            dataGrid.ItemsSource = itemsArray;
        }

    }
        

    public static void FillColumns(string query, DataGrid dataGrid)
    {
        var con = Configuration.getInstance().getConnection();

        SqlCommand command = new SqlCommand(query, con);

        SqlDataAdapter adapter = new SqlDataAdapter(command);

        DataTable dataTable = new DataTable();

        adapter.Fill(dataTable);

        dataGrid.Columns.Clear();
        dataGrid.ItemsSource = null;
        dataGrid.AutoGenerateColumns = false;

        for (var i = 0; i < dataTable.Columns.Count; i++)
        {
            if (dataTable.Columns[i].ColumnName.ToString().Contains("Description"))
            {
                dataGrid.Columns.Add(new DataGridTextColumn()
                {
                    Header = dataTable.Columns[i].ColumnName,
                    Binding = new Binding { Path = new PropertyPath(dataTable.Columns[i].ColumnName) },
                    Width = new DataGridLength(200),
                    ElementStyle = new Style(typeof(TextBlock))
                    {
                        Setters =
                            {
                                new Setter(TextBlock.TextWrappingProperty, TextWrapping.Wrap)
                            }
                    }
                });

                continue;
            }


            dataGrid.Columns.Add(new DataGridTextColumn()
            {
                Header = dataTable.Columns[i].ColumnName,
                Binding = new Binding { Path = new PropertyPath(dataTable.Columns[i].ColumnName) }
            });
        }
    }

    
    public static void ExecuteQuery(XamlRoot xamlRoot, string query)
    {
        var connection = Configuration.getInstance().getConnection();

        var command = new SqlCommand(query, connection);

        try
        {
            command.ExecuteNonQuery();
        }
        catch (Exception error)
        {
            ShowErrorMessage(xamlRoot, error.Message);
            return;
        }
    }



    public static void FillComboBoxProjectID(XamlRoot xamlRoot, ComboBox comboBox)
    {
        var con = Configuration.getInstance().getConnection();
        var cmd = new SqlCommand("select ProjectID, ProjectName from Project where ManagerID = @idd", con);
        cmd.Parameters.AddWithValue("@idd", UserDL.CurrentUser.UserID);
        SqlDataReader reader = cmd.ExecuteReader();
        // extract data
        while (reader.Read())
        {
            comboBox.Items.Add(reader["ProjectID"] + "\t" + reader["ProjectName"]);
        }
        reader.Close();
    }

    public static void ExecuteQuery(string query)
    {
        var con = Configuration.getInstance().getConnection();
        var cmd = new SqlCommand(query, con);
        cmd.ExecuteNonQuery();
    }


}



