﻿using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;

namespace Project_Management_Tool.Views;

public sealed partial class ViewMembersPage : Page
{
    public ViewMembersViewModel ViewModel
    {
        get;
    }

    public ViewMembersPage()
    {
        ViewModel = App.GetService<ViewMembersViewModel>();
        InitializeComponent();

        var query = "SELECT  PT.ProjectTeamID, PT.ProjectID, ProjectName, PTM.EmployeeID, (SELECT CONCAT(FirstName, ' ', LastName) FROM Users WHERE UserID = PTM.EmployeeID) AS EmployeeName, (SELECT Value FROM Lookup WHERE ID = Role) AS EmployeeRole, COALESCE(CONVERT(varchar(10), AssignmentDate, 105), 'NULL') AS AssignmentDate  FROM ProjectTeam PT JOIN [ProjectTeamMember] PTM ON PT.ProjectTeamID = PTM.ProjectTeamID JOIN Project P ON P.ProjectID = PT.ProjectID";

        Helper.FillColumns(query, dataGrid);

        Helper.FillDataGrid(query, dataGrid, "ProjectTeamMember");
    }
}
