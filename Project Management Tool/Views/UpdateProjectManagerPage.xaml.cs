﻿using Microsoft.UI.Xaml.Controls;
using Project_Management_Tool.BL;
using Microsoft.UI.Xaml;
using Project_Management_Tool.DL;

using Project_Management_Tool.ViewModels;
using Microsoft.UI.Xaml.Navigation;

#pragma warning disable CS8602
#pragma warning disable CS8604


namespace Project_Management_Tool.Views;

public sealed partial class UpdateProjectManagerPage : Page
{
    private readonly DateTimeOffset InitialDate;
    private string? LastIdSearched;
    private readonly string UserType = "4";
    
    public UpdateProjectManagerViewModel ViewModel
    {
        get;
    }

    public UpdateProjectManagerPage()
    {
        ViewModel = App.GetService<UpdateProjectManagerViewModel>();
        InitializeComponent();

        InitialDate = DateOfBirth_DatePicker.Date;

        var query = "SELECT [Value] FROM [Lookup] WHERE [Category] = 'Gender'";
        Helper.FillComboBox(XamlRoot, query, Gender_ComboBox, "Gender");
    }

    protected override void OnNavigatedTo(NavigationEventArgs e)
    {

        base.OnNavigatedTo(e);

        var user = (User)e.Parameter;

        if (user is null)
        {
            return;
        }

        FillValues(user);
        LastIdSearched = user.UserID;

        Id_TextBox.IsReadOnly = true;

        Visibility visibility = Visibility.Collapsed;
        Search_Button.Visibility = visibility;

    }

    private void FillValues(User user)
    {
        Id_TextBox.Text = user.UserID.ToString();
        FirstName_TextBox.Text = user.FirstName.ToString();

        if (user.LastName != "NULL")
        {
            LastName_TextBox.Text = user.LastName.ToString();
        }

        Email_TextBox.Text = user.Email.ToString();

        if (user.PhoneNumber != "NULL")
        {
            PhoneNumber_TextBox.Text = user.PhoneNumber.ToString();
        }


        if (user.DateOfBirth != "NULL")
        {
            DateOfBirth_DatePicker.SelectedDate = DateTimeOffset.Parse(user.DateOfBirth);
        }


        if (user.Gender != "NULL")
        {
            if (user.Gender == "1" || user.Gender == "2")
            {
                Gender_ComboBox.SelectedIndex = int.Parse(user.Gender) - 1;
            }
            else
            {
                Gender_ComboBox.SelectedIndex = user.Gender == "Male" ? 0 : 1;
            }

        }


        if (user.Username != null)
        {
            Username_TextBox.Text = user.Username.ToString();

        }
        if (user.Password != null)
        {
            Password_TextBox.Password = user.Password.ToString();
        }

        if (user.Username is null && user.Password is null)
        {
            var u = UserDL.GetUserByID(XamlRoot, user.UserID, UserType);
            if (u is null) return;

            Username_TextBox.Text = u.Username.ToString();
            Password_TextBox.Password = u.Password.ToString();
        }

    }

    private void Clear()
    {
        Id_TextBox.Text = string.Empty;
        FirstName_TextBox.Text = string.Empty;
        LastName_TextBox.Text = string.Empty;
        Gender_ComboBox.SelectedIndex = -1;
        DateOfBirth_DatePicker.SelectedDate = null;
        PhoneNumber_TextBox.Text = string.Empty;
        Email_TextBox.Text = string.Empty;
        Username_TextBox.Text = string.Empty;
        Password_TextBox.Password = string.Empty;

    }

    private void Search_Button_Click(object sender, RoutedEventArgs e)
    {
        var UserID = Id_TextBox.Text;

        if (UserID == "")
        {
            Helper.ShowErrorMessage(XamlRoot, "Please enter a valid ID");
            return;
        }

        LastIdSearched = UserID;


        var user = UserDL.GetUserByID(XamlRoot, UserID, UserType);

        if (user is null)
        {
            Helper.ShowErrorMessage(XamlRoot, "User not found");
            Clear();
            return;
        }

        FillValues(user);
    }

    private void Update_Button_Click(object sender, RoutedEventArgs e)
    {
        var UserID = Id_TextBox.Text;

        if (UserID == "")
        {
            Helper.ShowErrorMessage(XamlRoot, "ID Field is Empty, Please Search Again");
            Clear();
            return;
        }

        if (LastIdSearched != UserID)
        {
            Helper.ShowErrorMessage(XamlRoot, "You Changed the ID Field, Please Search Again");
            Clear();
            return;
        }

        var FirstName = FirstName_TextBox.Text;

        if (!ValidationManager.ValidateFirstName(XamlRoot, FirstName))
            return;

        var LastName = LastName_TextBox.Text;

        if (!ValidationManager.ValidateLastName(XamlRoot, LastName))
            return;

        var DateOfBirth = DateOfBirth_DatePicker.Date;

        if (DateOfBirth > DateTime.Now)
        {
            Helper.ShowErrorMessage(XamlRoot, "Invalid Date of Birth. Please enter a date in the past.");
            return;
        }

        var Gender = (Gender_ComboBox.SelectedIndex + 1).ToString();

        var Email = Email_TextBox.Text;

        if (!ValidationManager.ValidateEmail(XamlRoot, Email))
            return;

        var PhoneNumber = PhoneNumber_TextBox.Text;

        if (!ValidationManager.ValidateContact(XamlRoot, PhoneNumber))
            return;


        var Username = Username_TextBox.Text;

        if (!ValidationManager.ValidateUsername(XamlRoot, Username))
            return;

        var Password = Password_TextBox.Password;

        if (!ValidationManager.ValidatePassword(XamlRoot, Password))
            return;



        var user = new User(UserID, FirstName, LastName, DateOfBirth.ToString(), Gender, Email, PhoneNumber, Username, Password);

        UserDL.UpdateUserInDatabase(XamlRoot, user, InitialDate, DateOfBirth, UserType);

        Helper.ShowMessageBox(XamlRoot, "Success", "Updated Successfully");

        Frame.Navigate(typeof(ViewProjectManagersPage));
    }
}
