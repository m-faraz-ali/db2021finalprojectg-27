﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;
using System.Text;
using System.Threading.Tasks;
using Project_Management_Tool.BL;
using Project_Management_Tool.DL;

namespace Project_Management_Tool.Views;
internal class DL
{

    public static void AddProject(Project project)
    {
        //TODO
        var con = Configuration.getInstance().getConnection();
        SqlCommand cmd = new SqlCommand("Insert into Project values (@ProjectName, @ProjectDescription, @StartDate, @EndDate, (select id from Lookup where Category ='ProjectStatus' and Value='Not Started'), @ManagerID)", con);
        cmd.Parameters.AddWithValue("@ProjectName", project.ProjectName);
        cmd.Parameters.AddWithValue("@ProjectDescription", project.ProjectDescription);
        cmd.Parameters.AddWithValue("@StartDate", project.StartDate);
        cmd.Parameters.AddWithValue("@EndDate", project.EndDate);
        cmd.Parameters.AddWithValue("@ManagerID", int.Parse(project.ManagerID));
        cmd.ExecuteNonQuery();

        cmd = new SqlCommand("select max(ProjectID) from Project", con);
        var reader = cmd.ExecuteReader();
        reader.Read();
        int id = reader.GetInt32(0);
        reader.Close();

        cmd = new SqlCommand("Insert into ProjectTeam values (@ProjectID, @Date)", con);
        cmd.Parameters.AddWithValue("@ProjectID", id);
        cmd.Parameters.AddWithValue("@Date", DateTime.Now.ToString());
        cmd.ExecuteNonQuery();
        
        
    }
    public static void UpdateProjectById(string id, Project newProj)
    {
        var con = Configuration.getInstance().getConnection();
        SqlCommand cmd = new SqlCommand("Update Project set ProjectName=@ProjectName, ProjectDescription = @Description, StartDate=@StartDate, EndDate=@EndDate, ProjectStatus = 13, ManagerID=@ManagerID where ProjectID=@ProjectID", con);
        cmd.Parameters.AddWithValue("@ProjectName", newProj.ProjectName);
        cmd.Parameters.AddWithValue("@Description", newProj.ProjectDescription);
        cmd.Parameters.AddWithValue("@StartDate", newProj.StartDate);
        cmd.Parameters.AddWithValue("@EndDate", newProj.EndDate);
        cmd.Parameters.AddWithValue("@ManagerID", int.Parse(UserDL.CurrentUser.UserID));
        cmd.Parameters.AddWithValue("@ProjectID", int.Parse(id));
        cmd.BeginExecuteNonQuery();
    }
    public static void AddResource(string name, int type, int cost, string taskID)
    {
        var con = Configuration.getInstance().getConnection();
        SqlCommand cmd = new SqlCommand("Insert into Resource values (@ResourceName, @ResourceType, @Cost)", con);
        cmd.Parameters.AddWithValue("@ResourceName", name);
        cmd.Parameters.AddWithValue("@ResourceType", type);
        cmd.Parameters.AddWithValue("@Cost", cost);
        cmd.ExecuteNonQuery();

        cmd = new SqlCommand("select max(ResourceID) from Resource", con);
        var reader = cmd.ExecuteReader();
        reader.Read();
        int id = reader.GetInt32(0);
        reader.Close();

        cmd = new SqlCommand("Insert into TaskResource values (@TaskID, @ResourceID)", con);
        cmd.Parameters.AddWithValue("@TaskID", taskID);
        cmd.Parameters.AddWithValue("@ResourceID", id);
        cmd.ExecuteNonQuery();        
    }
}
