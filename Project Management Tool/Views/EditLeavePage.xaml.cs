﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.UI.Xaml.Controls;
using Project_Management_Tool.ViewModels;

namespace Project_Management_Tool.Views;
public sealed partial class EditLeavePage : Page
{
    public EditLeaveViewModel ViewModel
    {
        get;
    }
    
    public EditLeavePage()
    {
        ViewModel = App.GetService<EditLeaveViewModel>();
        InitializeComponent();
    }
    
    

}
