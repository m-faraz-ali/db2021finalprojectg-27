﻿using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;

using Project_Management_Tool.BL;
using Project_Management_Tool.DL;

namespace Project_Management_Tool.Views;

public sealed partial class AddProjectManagerPage : Page
{
    private readonly DateTimeOffset InitialDate;
    public AddProjectManagerViewModel ViewModel
    {
        get;
    }

    public AddProjectManagerPage()
    {
        ViewModel = App.GetService<AddProjectManagerViewModel>();
        InitializeComponent();

        InitialDate = DateOfBirth_DatePicker.Date;

        var query = "SELECT [Value] FROM [Lookup] WHERE [Category] = 'Gender'";
        Helper.FillComboBox(XamlRoot, query, Gender_ComboBox, "Gender");
    }

    private void AddRecord_Button_Click(object sender, Microsoft.UI.Xaml.RoutedEventArgs e)
    {
        var FirstName = FirstName_TextBox.Text;

        if (!ValidationManager.ValidateFirstName(XamlRoot, FirstName))
            return;

        var LastName = LastName_TextBox.Text;

        if (!ValidationManager.ValidateLastName(XamlRoot, LastName))
            return;

        var DateOfBirth = DateOfBirth_DatePicker.Date;

        if (DateOfBirth > DateTime.Now)
        {
            Helper.ShowErrorMessage(XamlRoot, "Invalid Date of Birth. Please enter a date in the past.");
            return;
        }

        var Gender = (Gender_ComboBox.SelectedIndex + 1).ToString();

        var Email = Email_TextBox.Text;

        if (!ValidationManager.ValidateEmail(XamlRoot, Email))
            return;

        var PhoneNumber = PhoneNumber_TextBox.Text;

        if (!ValidationManager.ValidateContact(XamlRoot, PhoneNumber))
            return;


        var Username = Username_TextBox.Text;

        if (UserDL.CheckUsername(XamlRoot, Username))
        {
            Helper.ShowErrorMessage(XamlRoot, "Username already exists. Please enter a different username.");
            return;
        }

        if (!ValidationManager.ValidateUsername(XamlRoot, Username))
            return;

        var Password = Password_TextBox.Password;

        if (!ValidationManager.ValidatePassword(XamlRoot, Password))
            return;


        var UserType = "4";
        var user = new User(FirstName, LastName, DateOfBirth.ToString(), Gender, Email, PhoneNumber, Username, Password);

        UserDL.AddUserToDatabase(XamlRoot, user, InitialDate, DateOfBirth, UserType);

        Helper.ShowMessageBox(XamlRoot, "Success", "Added to Database Successfully");

    }
}
