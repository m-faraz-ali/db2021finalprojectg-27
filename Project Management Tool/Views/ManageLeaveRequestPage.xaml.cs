﻿using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;
using Project_Management_Tool.BL;
using Project_Management_Tool.DL;


namespace Project_Management_Tool.Views;

public sealed partial class ManageLeaveRequestPage : Page
{
    public ManageLeaveRequestViewModel ViewModel
    {
        get;
    }

    public ManageLeaveRequestPage()
    {
        ViewModel = App.GetService<ManageLeaveRequestViewModel>();
        InitializeComponent();

        Refresh();
    }

    private void Refresh()
    {
        var query = "SELECT LeaveID, EmployeeID, (SELECT CONCAT(FirstName, ' ', LastName) FROM Users WHERE UserID = EmployeeID) AS EmployeeName, COALESCE(CONVERT(varchar(10), StartDate, 105), 'NULL') AS StartDate, COALESCE(CONVERT(varchar(10), EndDate, 105), 'NULL') AS EndDate, (SELECT Value FROM Lookup WHERE ID = LeaveType) AS LeaveType, (SELECT Value FROM Lookup WHERE ID = LeaveStatus) AS LeaveStatus FROM LeaveRequest where LeaveStatus = 10";
        Helper.FillColumns(query, dataGrid);
        Helper.FillDataGrid(query, dataGrid, "LeaveRequest");
    }

    public void Approve_Leave(object sender, Microsoft.UI.Xaml.RoutedEventArgs e)
    {
        var SelectedIndex = dataGrid.SelectedIndex;

        if (SelectedIndex < 0)
        {
            Helper.ShowErrorMessage(XamlRoot, "Please Select a Row to Approve");
            return;
        }
        var SelectedItem = (LeaveRequest)dataGrid.SelectedItem;

        SelectedItem.LeaveStatus = "11";

        var query = $"UPDATE LeaveRequest SET LeaveStatus = {SelectedItem.LeaveStatus}, ManagerID = {UserDL.CurrentUser.UserID}  WHERE EmployeeID={SelectedItem.EmployeeID};";

        Helper.ExecuteQuery(XamlRoot, query);

        Refresh();

    }
    public void Reject_Leave(object sender, Microsoft.UI.Xaml.RoutedEventArgs e)
    {
        var SelectedIndex = dataGrid.SelectedIndex;

        if (SelectedIndex < 0)
        {
            Helper.ShowErrorMessage(XamlRoot, "Please Select a Row to Reject");
            return;
        }

        var SelectedItem = (LeaveRequest)dataGrid.SelectedItem;

        SelectedItem.LeaveStatus = "12";

        var query = $"UPDATE LeaveRequest SET LeaveStatus = {SelectedItem.LeaveStatus}, ManagerID = {UserDL.CurrentUser.UserID} WHERE EmployeeID={SelectedItem.EmployeeID};";

        Helper.ExecuteQuery(XamlRoot, query);

        Refresh();
    }
}
