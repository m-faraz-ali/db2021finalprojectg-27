﻿using Microsoft.UI.Xaml;
using System.Diagnostics;
using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;
using Project_Management_Tool.BL;
using Project_Management_Tool.DL;
using System.Data.SqlClient;
using System.Data;

namespace Project_Management_Tool.Views;

public sealed partial class UpdateTaskPage : Page
{
    public UpdateTaskViewModel ViewModel
    {
        get;
    }

    DateTimeOffset d1, d2;
    public UpdateTaskPage()
    {
        ViewModel = App.GetService<UpdateTaskViewModel>();
        InitializeComponent();
        Helper.FillComboBoxProjectID(XamlRoot, ProjectID_ComboBox);
        string query = "select ID, Value from Lookup where Category = 'TaskStatus'";
        Helper.FillComboBox(XamlRoot, query, TaskStatus_ComboBox, "TaskStatus");
        query = "select ID, Value from Lookup where Category = 'TaskPriority'";
        Helper.FillComboBox(XamlRoot, query, TaskPriority_ComboBox, "TaskPriority");
        d1 = StartDate_DatePicker.Date;
        d2 = EndDate_DatePicker.Date;
        //query = $"select T.TaskID, T.TaskName from Task T left join Project PJ on T.ProjectID = PJ.ProjectID where PJ.ManagerID = {UserDL.CurrentUser.UserID}";
        //Helper.FillComboBox(XamlRoot, query, TaskID_ComboBox, "TaskID");
    }

    public void UpdateTask_ButtonClick(object sender, RoutedEventArgs e)
    {
        var taskId = TaskID_ComboBox.SelectedValue.ToString().Split("\t")[0];
        var projId = ProjectID_ComboBox.SelectedValue.ToString().Split("\t")[0];
        var taskName = TaskName_TextBox.Text;
        var taskDesc = TaskDescription_TextBox.Text;
        var taskStatus = TaskStatus_ComboBox.SelectedValue.ToString().Split("\t")[0];
        var taskPriority = TaskPriority_ComboBox.SelectedValue.ToString().Split("\t")[0];
        var startDate = StartDate_DatePicker.Date;
        var endDate = EndDate_DatePicker.Date;
        var assignedTo = AssignedTo_ComboBox.SelectedValue.ToString().Split("\t")[0];

        if (taskId == null || projId == null || taskName == null || taskDesc == null || taskStatus == null || taskPriority == null || startDate == d1 || endDate == d2)
        {
            Helper.ShowErrorMessage(XamlRoot, "Incomplete Input!");
        }
        else if (startDate > endDate)
        {
            Helper.ShowErrorMessage(XamlRoot, "Invalid Date!");
        }
        else
        {
            var query = $"update Task set TaskName = '{taskName}', TaskDescription = '{taskDesc}', TaskStatus = {taskStatus}, TaskPriority= {taskPriority}, StartDate='{startDate.Date}', EndDate='{endDate.Date}', AssignedTo={assignedTo} where TaskID = {taskId}";
            Debug.WriteLine(query);
            try
            {
                Helper.ExecuteQuery(query);
            }
            catch (Exception error)
            {
                Helper.ShowErrorMessage(XamlRoot, error.Message + "999");
                return;
            }
            Helper.ShowMessageBox(XamlRoot, "Success", "Task Updated Successfully!");
        }
    }

    private void TaskID_ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        var TaskID = TaskID_ComboBox.SelectedValue.ToString().Split("\t")[0];

        var query = $"select AssignedTo AS EmployeeID, (Select CONCAT(FirstName, ' ', LastName) FROM Users Where UserID = AssignedTo) AS EmployeeName, TaskName, TaskDescription, (Select Value From Lookup WHERE ID = TaskStatus) AS TaskStatus, TaskStatus AS TaskStatusID, (Select Value From Lookup WHERE ID = TaskPriority) AS TaskPriority, TaskPriority AS TaskPriorityID, StartDate, EndDate  from Task where TaskID = {TaskID}";

        var con = Configuration.getInstance().getConnection();

        var cmd = new SqlCommand(query, con);

        try
        {
            var adapter = new SqlDataAdapter(cmd);

            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            var row = dataTable.Rows[0];

            var TaskName = row["TaskName"].ToString();
            var TaskDescription = row["TaskDescription"].ToString();
            var TaskStatus = row["TaskStatus"].ToString();
            var TaskPriority = row["TaskPriority"].ToString();
            var StartDate = row["StartDate"].ToString();
            var EndDate = row["EndDate"].ToString();
            var TaskStatusID = row["TaskStatusID"].ToString();
            var TaskPriorityID = row["TaskPriorityID"].ToString();
            var EmployeeID = row["EmployeeID"].ToString();
            var EmployeeName = row["EmployeeName"].ToString();

            TaskName_TextBox.Text = TaskName;
            TaskDescription_TextBox.Text = TaskDescription;
            StartDate_DatePicker.SelectedDate = DateTime.Parse(StartDate);
            EndDate_DatePicker.SelectedDate = DateTime.Parse(EndDate);

            TaskPriority_ComboBox.SelectedValue = TaskPriorityID + "\t" + TaskPriority;
            TaskStatus_ComboBox.SelectedValue = TaskStatusID + "\t" + TaskStatus;
            AssignedTo_ComboBox.SelectedValue = EmployeeID + "\t" + EmployeeName;

        }
        catch (Exception exp)
        {
            Helper.ShowErrorMessage(XamlRoot, exp.Message);
        }
    }

    private void ProjectID_ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        var id = ProjectID_ComboBox.SelectedValue.ToString().Split("\t")[0];
        string query = $"select UserID , CONCAT(FirstName, ' ', LastName) as FullName from Users where UserID in (select PTM.EmployeeID from ProjectTeamMember as PTM inner join ProjectTeam as PM on PTM.ProjectTeamID = PM.ProjectTeamID where PM.ProjectID = '{id}')";
        Helper.FillComboBox(XamlRoot, query, AssignedTo_ComboBox, "AddMember");

        query = $"select TaskID, TaskName from Task where ProjectID = {id}";
        Helper.FillComboBox(XamlRoot, query, TaskID_ComboBox, "TaskID");


    }
}
