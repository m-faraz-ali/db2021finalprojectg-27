﻿using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;

namespace Project_Management_Tool.Views;

public sealed partial class AssignResourcePage : Page
{
    public AssignResourceViewModel ViewModel
    {
        get;
    }

    public AssignResourcePage()
    {
        ViewModel = App.GetService<AssignResourceViewModel>();
        InitializeComponent();
        Helper.FillComboBoxProjectID(XamlRoot, ProjectID_ComboBox);
        string query = "select ID, Value from Lookup where Category = 'ResourceType'";
        Helper.FillComboBox(XamlRoot, query, ResourceType_ComboBox, "ResourceType");
    }


    private void ProjectID_ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        var projId = ProjectID_ComboBox.SelectedItem.ToString().Split("\t")[0];
        string query = $"select TaskID, TaskName from Task where ProjectID = {projId}";
        Helper.FillComboBox(XamlRoot, query, TaskID_ComboBox, "TaskID");
    }

    private void AssignResource_ButtonClick(object sender, Microsoft.UI.Xaml.RoutedEventArgs e)
    {
        int cost;

        string name = ResourceName_TextBox.Text;

        int type = int.Parse(ResourceType_ComboBox.SelectedValue.ToString().Split("\t")[0]);
        string costS = Cost_TextBox.Text;

        string taskID = TaskID_ComboBox.SelectedValue.ToString().Split("\t")[0];


        try
        {
            cost = int.Parse(costS);
        }
        catch (Exception exp)
        {
            Helper.ShowErrorMessage(XamlRoot, "Invalid Cost Value!");
            return;
        }

        if (cost < 0)
        {
            Helper.ShowErrorMessage(XamlRoot, "Invalid Cost Value!");
            return;
        }

        if (name == null || type == null || costS == null || taskID == null)
        {
            Helper.ShowErrorMessage(XamlRoot, "Incomplete Input!");
        }
        else
        {
            try
            {
                DL.AddResource(name, type, cost, taskID);
            }
            catch (Exception error)
            {
                Helper.ShowErrorMessage(XamlRoot, error.Message);
                return;
            }
            Helper.ShowMessageBox(XamlRoot, "Success!", "Resource Assigned Successfully!");
        }
    }
}
