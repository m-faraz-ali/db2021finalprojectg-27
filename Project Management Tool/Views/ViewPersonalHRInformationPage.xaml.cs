﻿using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;
using Project_Management_Tool.DL;
using Windows.Media.SpeechSynthesis;

namespace Project_Management_Tool.Views;

public sealed partial class ViewPersonalHRInformationPage : Page
{
    public ViewPersonalHRInformationViewModel ViewModel
    {
        get;
    }

    public ViewPersonalHRInformationPage()
    {
        ViewModel = App.GetService<ViewPersonalHRInformationViewModel>();
        InitializeComponent();

        var query = $"Select FirstName, COALESCE(LastName, 'NULL') AS LastName, COALESCE(CONVERT(varchar(10), DateOfBirth, 105), 'NULL') AS DateOfBirth, COALESCE((SELECT Value FROM Lookup WHERE ID = Gender), 'NULL') AS Gender, Email, COALESCE(PhoneNumber, 'NULL') AS PhoneNumber FROM Users WHERE UserID = {UserDL.CurrentUser.UserID}";
        Helper.FillColumns(query, dataGrid);
        var query2 = $"Select UserID, FirstName, COALESCE(LastName, 'NULL') AS LastName, COALESCE(CONVERT(varchar(10), DateOfBirth, 105), 'NULL') AS DateOfBirth, COALESCE((SELECT Value FROM Lookup WHERE ID = Gender), 'NULL') AS Gender, Email, COALESCE(PhoneNumber, 'NULL') AS PhoneNumber FROM Users WHERE UserID = {UserDL.CurrentUser.UserID}";
        Helper.FillDataGrid(query2, dataGrid, "User");
    }
}
