﻿using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.ViewModels;

namespace Project_Management_Tool.Views;

public sealed partial class ViewTasksPage : Page
{
    public ViewTasksViewModel ViewModel
    {
        get;
    }

    public ViewTasksPage()
    {
        ViewModel = App.GetService<ViewTasksViewModel>();
        InitializeComponent();

        var query = "SELECT TaskID, TaskName, TaskDescription, ProjectID, (Select ProjectName FROM Project WHERE ProjectID = ProjectID) AS ProjectName, COALESCE(CONVERT(varchar(10), StartDate, 105), 'NULL') AS StartDate, COALESCE(CONVERT(varchar(10), EndDate, 105), 'NULL') AS EndDate, (SELECT Value FROM Lookup WHERE ID = TaskStatus) AS TaskStatus, (SELECT Value FROM Lookup WHERE ID = TaskPriority) AS TaskPriority, AssignedTo AS AssignedEmployeeID, (SELECT CONCAT(FirstName, ' ', LastName) FROM Users WHERE UserID = AssignedTo) AS AssignedEmployeeName   FROM Task;";

        Helper.FillColumns(query, dataGrid);

        Helper.FillDataGrid(query, dataGrid, "Task");
    }


}
