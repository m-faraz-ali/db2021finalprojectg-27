﻿using CommunityToolkit.Mvvm.ComponentModel;

using Microsoft.UI.Xaml.Controls;

using Project_Management_Tool.Contracts.Services;
using Project_Management_Tool.ViewModels;
using Project_Management_Tool.Views;

namespace Project_Management_Tool.Services;

public class PageService : IPageService
{
    private readonly Dictionary<string, Type> _pages = new();

    public PageService()
    {

        Configure<SettingsViewModel, SettingsPage>();
        Configure<AddEmployeeViewModel, AddEmployeePage>();
        Configure<ViewEmployeesViewModel, ViewEmployeesPage>();
        Configure<UpdateEmployeeViewModel, UpdateEmployeePage>();
        Configure<AddHRManagerViewModel, AddHRManagerPage>();
        Configure<UpdateHRManagerViewModel, UpdateHRManagerPage>();
        Configure<ViewHRManagersViewModel, ViewHRManagersPage>();
        Configure<AddProjectManagerViewModel, AddProjectManagerPage>();
        Configure<ViewProjectManagersViewModel, ViewProjectManagersPage>();
        Configure<UpdateProjectManagerViewModel, UpdateProjectManagerPage>();
        Configure<LoginViewModel, LoginPage>();

        Configure<ManageLeaveRequestViewModel, ManageLeaveRequestPage>();
        Configure<MarkAttendanceViewModel, MarkAttendancePage>();
        Configure<ViewPersonalHRInformationViewModel, ViewPersonalHRInformationPage>();
        Configure<ShellHRViewModel, ShellHRPage>();
        
        Configure<AddLeaveViewModel, AddLeavePage>();
        Configure<EditLeaveViewModel, EditLeavePage>();
        Configure<ShellEmpViewModel, ShellEmpPage>();

        Configure<ViewInformationViewModel, ViewInformationPage>();
        Configure<ViewLeaveViewModel, ViewLeavePage>();
        Configure<ViewTaskViewModel, ViewTaskPage>();
        Configure<AddMembersViewModel, AddMembersPage>();
        Configure<AddProjectsViewModel, AddProjectsPage>();
        Configure<AddTaskViewModel, AddTaskPage>();
        Configure<AssignResourceViewModel, AssignResourcePage>();
        Configure<UpdateProjectViewModel, UpdateProjectPage>();
        Configure<UpdateResourceViewModel, UpdateResourcePage>();
        Configure<UpdateTaskViewModel, UpdateTaskPage>();
        Configure<ViewMembersViewModel, ViewMembersPage>();
        Configure<ViewProjectsViewModel, ViewProjectsPage>();
        Configure<ViewResourcesViewModel, ViewResourcesPage>();
        Configure<ViewTasksViewModel, ViewTasksPage>();
        Configure<ShellPMViewModel, ShellPMPage>();
        Configure<AttendanceHistoryViewModel, AttendanceHistoryPage>();
    }

    public Type GetPageType(string key)
    {
        Type? pageType;
        lock (_pages)
        {
            if (!_pages.TryGetValue(key, out pageType))
            {
                throw new ArgumentException($"Page not found: {key}. Did you forget to call PageService.Configure?");
            }
        }

        return pageType;
    }

    private void Configure<VM, V>()
        where VM : ObservableObject
        where V : Page
    {
        lock (_pages)
        {
            var key = typeof(VM).FullName!;
            if (_pages.ContainsKey(key))
            {
                throw new ArgumentException($"The key {key} is already configured in PageService");
            }

            var type = typeof(V);
            if (_pages.Any(p => p.Value == type))
            {
                throw new ArgumentException($"This type is already configured with key {_pages.First(p => p.Value == type).Key}");
            }

            _pages.Add(key, type);
        }
    }
}
