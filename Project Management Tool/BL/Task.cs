﻿

namespace Project_Management_Tool.BL;
public class Task
{
    public string? TaskID
    {
        get; set;
    }

    public string? TaskName
    {
        get; set;
    }

    public string? TaskDescription
    {
        get; set;
    }

    public string? ProjectID
    {
        get; set;
    }

    public string? ProjectName
    {
        get; set;
    }

    public string? StartDate
    {
        get; set;
    }

    public string? EndDate
    {
        get; set;
    }

    public string? TaskStatus
    {
        get; set;
    }

    public string? TaskPriority
    {
        get; set;
    }

    public string? AssignedEmployeeID
    {
        get; set;
    }

    public string? AssignedEmployeeName
    {
        get; set;
    }


    public Task(string TaskID, string TaskName, string TaskDescription, string ProjectID, string ProjectName, string StartDate, string EndDate, string TaskStatus, string TaskPriority, string AssignedEmployeeID, string AssignedEmployeeName)
    {
        this.TaskID = TaskID;
        this.TaskName = TaskName;
        this.TaskDescription = TaskDescription;
        this.ProjectID = ProjectID;
        this.ProjectName = ProjectName;
        this.StartDate = StartDate;
        this.EndDate = EndDate;
        this.TaskStatus = TaskStatus;
        this.TaskPriority = TaskPriority;
        this.AssignedEmployeeID = AssignedEmployeeID;
        this.AssignedEmployeeName = AssignedEmployeeName;
    }

}
