﻿
namespace Project_Management_Tool.BL;
public class Project
{
    public string? ProjectID
    {
        get; set;
    }
    public string? ProjectName
    {
        get; set;
    }
    public string? ProjectDescription
    {
        get; set;
    }
    public string? StartDate
    {
        get; set;
    }
    public string? EndDate
    {
        get; set;
    }
    public string? ProjectStatus
    {
        get; set;
    }
    public string? ManagerID
    {
        get; set;
    }

    public string? ManagerName
    {
        get; set;
    }


    public Project(string ProjectID, string ProjectName, string ProjectDescription, string StartDate, string EndDate, string ProjectStatus, string ManagerID, string ManagerName)
    {
        this.ProjectID = ProjectID;
        this.ProjectName = ProjectName;
        this.ProjectDescription = ProjectDescription;
        this.StartDate = StartDate;
        this.EndDate = EndDate;
        this.ProjectStatus = ProjectStatus;
        this.ManagerID = ManagerID;
        this.ManagerName = ManagerName;
    }

    public Project()
    {

    }

}
