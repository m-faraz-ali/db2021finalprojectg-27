﻿
namespace Project_Management_Tool.BL;
public class ProjectTeamMember : ProjectTeam
{
    public string? EmployeeID
    {
        get; set;
    }

    public string? EmployeeName
    {
        get; set;
    }

    public string? EmployeeRole
    {
        get; set;
    }

    public ProjectTeamMember(string ProjectTeamID, string ProjectID, string ProjectName, string EmployeeID, string EmployeeName, string EmployeeRole, string AssignmentDate)
        : base(ProjectTeamID, ProjectID, ProjectName, AssignmentDate)
    {
        this.EmployeeID = EmployeeID;
        this.EmployeeName = EmployeeName;
        this.EmployeeRole = EmployeeRole; 
    }
    
}
