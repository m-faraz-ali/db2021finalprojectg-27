﻿
namespace Project_Management_Tool.BL;

public class Attendance
{
    public string EmployeeID 
    { 
        get; set; 
    }

    public string EmployeeName
    {
        get; set;
    }

    public string AttendanceDate
    {
        get; set;
    }
    
    public string AttendanceStatus
    {
        get; set;
    }

    public Attendance(string EmployeeID, string EmployeeName, string AttendanceDate, string AttendanceStatus)
    {
        this.EmployeeID = EmployeeID;
        this.EmployeeName = EmployeeName;
        this.AttendanceDate = AttendanceDate;
        this.AttendanceStatus = AttendanceStatus;
    }
}