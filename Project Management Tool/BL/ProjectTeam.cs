﻿
namespace Project_Management_Tool.BL;

public class ProjectTeam
{
    public string? ProjectTeamID
    {
        get; set;
    }

    public string? ProjectID
    {
        get; set;    
    }

    public string? ProjectName
    {
        get; set;
    }

    public string? AssignmentDate
    {
        get; set;
    }

    public ProjectTeam(string ProjectTeamID, string ProjectID, string ProjectName, string AssignmentDate)
    {
        this.ProjectTeamID = ProjectTeamID;
        this.ProjectID = ProjectID;
        this.ProjectName = ProjectName; 
        this.AssignmentDate = AssignmentDate; 
    }
}
