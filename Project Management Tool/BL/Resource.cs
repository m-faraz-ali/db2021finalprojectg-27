﻿
namespace Project_Management_Tool.BL;
public class Resource
{
    public string? ResourceID
    {
        get;
        set;
    }
    
    public string? ResourceName
    {
        get;
        set;
    }
    public string? ResourceType
    {
        get;
        set;
    }
    
    public string? ResourceCost
    {
        get;
        set;
    }

    public Resource(string ResourceID, string ResourceName, string ResourceType, string ResourceCost)
    {
        this.ResourceID = ResourceID;
        this.ResourceName = ResourceName;
        this.ResourceType = ResourceType;
        this.ResourceCost = ResourceCost;
    }
}
