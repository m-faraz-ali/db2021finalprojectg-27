﻿

namespace Project_Management_Tool.BL;

public class TaskResource : Resource
{
    public string? TaskID
    {
        get;
        set;
    }

    public string? TaskName
    {
        get;
        set;
    }


    public TaskResource(string TaskID, string TaskName, string ResourceID, string ResourceName, string ResourceType, string ResourceCost)
        : base(ResourceID, ResourceName, ResourceType, ResourceCost)
    {
        this.TaskID = TaskID;
        this.TaskName = TaskName;
    }
    
}
