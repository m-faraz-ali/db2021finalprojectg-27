﻿

namespace Project_Management_Tool.BL;


public class User
{
    public string? UserID { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string DateOfBirth { get; set; }
    public string Gender { get; set; }
    public string Email { get; set; }
    public string PhoneNumber { get; set; }
    public string? UserType { get; set; }

    public string? Username { get; set; }

    public string? Password { get; set; }


    public User(string FirstName, string LastName, string DateOfBirth, string Gender, string Email, string PhoneNumber, string Username, string Password)
    {
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.DateOfBirth = DateOfBirth;
        this.Gender = Gender;
        this.Email = Email;
        this.PhoneNumber = PhoneNumber;
        this.Username = Username;
        this.Password = Password;

    }

    public User(string UserID, string FirstName, string LastName, string DateOfBirth, string Gender, string Email, string PhoneNumber, string Username, string Password)
    {
        this.UserID = UserID;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.DateOfBirth = DateOfBirth;
        this.Gender = Gender;
        this.Email = Email;
        this.PhoneNumber = PhoneNumber;
        this.Username = Username;
        this.Password = Password;
    }

    public User(string UserID, string FirstName, string LastName, string DateOfBirth, string Gender, string Email, string PhoneNumber)
    {
        this.UserID = UserID;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.DateOfBirth = DateOfBirth;
        this.Gender = Gender;
        this.Email = Email;
        this.PhoneNumber = PhoneNumber;

    }

}
