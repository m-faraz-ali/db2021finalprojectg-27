﻿
namespace Project_Management_Tool.BL;
public class LeaveRequest
{
    public string? LeaveID
    {
        get; set;
    }

    public string EmployeeID
    {
        get; set;
    }

    public string? EmployeeName
    {
        get; set;
    }
    public string StartDate
    {
        get; set;
    }
    public string EndDate
    {
        get; set;
    }
    public string LeaveType
    {
        get; set;
    }
    public string LeaveStatus
    {
        get; set;
    }

    public string? ManagerID
    {
        get; set;
    }

    public LeaveRequest(string LeaveID, string EmployeeID, string EmployeeName, string StartDate, string EndDate, string LeaveType, string LeaveStatus)
    {

        this.LeaveID = LeaveID;
        this.EmployeeID = EmployeeID;
        this.EmployeeName = EmployeeName;
        this.StartDate = StartDate;
        this.EndDate = EndDate;
        this.LeaveType = LeaveType;
        this.LeaveStatus = LeaveStatus;
    }

    public LeaveRequest(string EmployeeID, string StartDate, string EndDate, string LeaveType, string LeaveStatus)
    {
        this.EmployeeID = EmployeeID;
        this.StartDate = StartDate;
        this.EndDate = EndDate;
        this.LeaveType = LeaveType;
        this.LeaveStatus = LeaveStatus;
    }
}
