﻿namespace Project_Management_Tool.Contracts.Services;

public interface IActivationService
{
    Task ActivateAsync(object activationArgs);
}
