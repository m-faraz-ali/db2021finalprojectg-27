﻿namespace Project_Management_Tool.Contracts.Services;

public interface IPageService
{
    Type GetPageType(string key);
}
