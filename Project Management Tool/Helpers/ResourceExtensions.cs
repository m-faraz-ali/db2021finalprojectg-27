﻿using Microsoft.Windows.ApplicationModel.Resources;

namespace Project_Management_Tool.Helpers;

public static class ResourceExtensions
{
    private static readonly ResourceLoader _resourceLoader = new();

    public static string GetLocalized(this string resourceKey) => _resourceLoader.GetString(resourceKey);
}
