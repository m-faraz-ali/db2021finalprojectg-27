﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.UI.Xaml;
using Project_Management_Tool.BL;
using Project_Management_Tool.Views;

namespace Project_Management_Tool.DL;
internal class LeaveRequestDL
{
    public static void AddLeaveRequestToDB(XamlRoot xamlRoot, LeaveRequest leaveRequest)
    {
        //query to insert data into database
        string query = $"INSERT INTO LeaveRequest " +
            $"(EmployeeID, StartDate, EndDate, LeaveType, LeaveStatus) " +
            $"VALUES ({leaveRequest.EmployeeID}, '{leaveRequest.StartDate}', '" +
            $"{leaveRequest.EndDate}', {leaveRequest.LeaveType}, {leaveRequest.LeaveStatus} " +
            $")";

        Helper.ExecuteQuery(xamlRoot, query);
    }
}
