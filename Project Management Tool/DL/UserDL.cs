﻿
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Microsoft.UI.Xaml;
using Project_Management_Tool.BL;
using Project_Management_Tool.Views;


namespace Project_Management_Tool.DL;
internal class UserDL
{
    public static User? CurrentUser;
    public static void AddUserToDatabase(XamlRoot xamlRoot, User user, DateTimeOffset InitialDate, DateTimeOffset DateOfBirth, string UserType)
    {
       
        var query = "INSERT INTO Users ";
        query += "(FirstName, ";
        query = (user.LastName != "") ? query += "LastName, " : query;
        query = (user.PhoneNumber != "") ? query += "PhoneNumber, " : query;
        query += "Email";
        query = (DateOfBirth != InitialDate) ? query += ", DateOfBirth" : query;
        query = (int.Parse(user.Gender) > 0) ? query += ", Gender" : query;
        query += ", UserType) VALUES (";
        query += string.Concat("\'", user.FirstName, "\', ");
        query = (user.LastName != "") ? query += string.Concat(" \'", user.LastName, "\', ") : query;
        query = (user.PhoneNumber != "") ? query += string.Concat(" \'", user.PhoneNumber, "\', ") : query;
        query += string.Concat(" \'", user.Email, "\'");
        query = (DateOfBirth != InitialDate) ? query += $", '{user.DateOfBirth}'" : query;
        query = (int.Parse(user.Gender) > 0) ? query += string.Concat(",  ", user.Gender) : query;
        query += $", {UserType});";

        Helper.ExecuteQuery(xamlRoot, query);

        query = $"INSERT INTO Credentials (UserId, Username, Password) VALUES ((SELECT MAX(UserId) FROM Users), '{user.Username}', '{user.Password}')";

        Helper.ExecuteQuery(xamlRoot, query);
    }

    public static void UpdateUserInDatabase(XamlRoot xamlRoot, User user, DateTimeOffset InitialDate, DateTimeOffset DateOfBirth, string UserType)
    {
        var query = "UPDATE Users SET ";
        query += $"FirstName = '{user.FirstName}', ";
        query = (user.LastName != "") ? query += $"LastName = '{user.LastName}', " : query;
        query = (user.PhoneNumber != "") ? query += $"PhoneNumber = '{user.PhoneNumber}', " : query;
        query += $"Email = '{user.Email}'";
        query = (DateOfBirth != InitialDate) ? query += $", DateOfBirth = '{user.DateOfBirth}'" : query;
        query = (int.Parse(user.Gender) > 0) ? query += $", Gender = {user.Gender}" : query;
        query += $", UserType = {UserType} WHERE UserID = {user.UserID};";

        try
        {
            Helper.ExecuteQuery(xamlRoot, query);
        }
        catch (Exception)
        {
            return;
        }
        try
        {
            query = $"UPDATE Credentials SET Username = '{user.Username}', Password = '{user.Password}' WHERE UserID = {user.UserID}";
            Helper.ExecuteQuery(xamlRoot, query);
        }
        catch (Exception)
        {
            return;
        }
    }

    public static bool CheckCredentials(XamlRoot xamlRoot, string Username, string Password)
    {
        var query = $"SELECT COUNT(*) FROM Credentials WHERE Username = '{Username}' AND Password = '{Password}'";

        var connection = Configuration.getInstance().getConnection();

        var command = new SqlCommand(query, connection);

        var result = command.ExecuteScalar();

        return (int)result > 0;
        

    }

    public static bool CheckUsername(XamlRoot xamlRoot, string Username)
    {
        var query = $"SELECT COUNT(*) FROM Credentials WHERE Username = '{Username}'";

        var connection = Configuration.getInstance().getConnection();

        var command = new SqlCommand(query, connection);

        var result = command.ExecuteScalar();

        return (int)result > 0;

    }

    public static User? GetUserByID(XamlRoot xamlRoot, string UserID, string UserType)
    {
        var con = Configuration.getInstance().getConnection();
        var query = $"SELECT U.UserID, FirstName, LastName, DateOfBirth, Gender, Email, PhoneNumber, C.UserName, C.Password FROM Users U JOIN Credentials C ON U.UserID = C.UserID WHERE U.UserType = {UserType} AND U.UserID = {UserID}";

        var cmd = new SqlCommand(query, con);

        try
        {
            var adapter = new SqlDataAdapter(cmd);

            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            if (dataTable.Rows.Count <= 0)
            {
                return null;
            }

            var dataRow = dataTable.Rows[0];

            var UserId = dataRow["UserID"].ToString();
            var FirstName = dataRow["FirstName"].ToString();
            var LastName = dataRow["LastName"].ToString();
            var DateOfBirth = dataRow["DateOfBirth"].ToString();
            var Gender = dataRow["Gender"].ToString();
            var Email = dataRow["Email"].ToString();
            var PhoneNumber = dataRow["PhoneNumber"].ToString();
            var Username = dataRow["Username"].ToString();
            var Password = dataRow["Password"].ToString();

            var user = new User(UserId, FirstName, LastName, DateOfBirth, Gender, Email, PhoneNumber, Username, Password);

            return user;

        }
        catch (Exception exp)
        {
            Helper.ShowErrorMessage(xamlRoot, exp.Message);
            return null;
        }
    }

    public static User? GetUserByID(XamlRoot xamlRoot, string UserID)
    {
        var con = Configuration.getInstance().getConnection();
        var query = $"SELECT U.UserID, FirstName, LastName, DateOfBirth, Gender, Email, PhoneNumber, C.UserName, C.Password, U.UserType FROM Users U JOIN Credentials C ON U.UserID = C.UserID WHERE U.UserID = {UserID}";

        Debug.WriteLine(query);

        var cmd = new SqlCommand(query, con);

        try
        {
            var adapter = new SqlDataAdapter(cmd);

            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            if (dataTable.Rows.Count <= 0)
            {
                return null;
            }

            var dataRow = dataTable.Rows[0];

            var UserId = dataRow["UserID"].ToString();
            var FirstName = dataRow["FirstName"].ToString();
            var LastName = dataRow["LastName"].ToString();
            var DateOfBirth = dataRow["DateOfBirth"].ToString();
            var Gender = dataRow["Gender"].ToString();
            var Email = dataRow["Email"].ToString();
            var PhoneNumber = dataRow["PhoneNumber"].ToString();
            var Username = dataRow["Username"].ToString();
            var Password = dataRow["Password"].ToString();
            var UserType = dataRow["UserType"].ToString();

            var user = new User(UserId, FirstName, LastName, DateOfBirth, Gender, Email, PhoneNumber, Username, Password);
            user.UserType = UserType;

            return user;

        }
        catch (Exception exp)
        {
            Helper.ShowErrorMessage(xamlRoot, exp.Message);
            return null;
        }
    }

    public static User? GetUserByUsername(XamlRoot xamlRoot, string Username)
    {
        var query = $"SELECT UserID FROM Credentials WHERE Username = '{Username}'";


        var con = Configuration.getInstance().getConnection();

        var cmd = new SqlCommand(query, con);

        try
        {
            var adapter = new SqlDataAdapter(cmd);

            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            if (dataTable.Rows.Count <= 0)
            {
                return null;
            }

            var dataRow = dataTable.Rows[0];

            var UserId = dataRow["UserID"].ToString();

            var user = GetUserByID(xamlRoot, UserId);

            if (user is null)
            {
                return null;
            }

            return user;


        }
        catch (Exception exp)
        {
            Helper.ShowErrorMessage(xamlRoot, exp.Message);
            return null;
        }
    }

}
