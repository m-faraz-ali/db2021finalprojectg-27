﻿#pragma checksum "C:\Users\affan\Downloads\Compressed\db2021finalprojectg-27-main_2\db2021finalprojectg-27-main\Project Management Tool\Views\AssignResourcePage.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "46562A2BE9935D853FDB44E492A8B95D41E819A82B7966BA620BB3697D52AA86"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Project_Management_Tool.Views
{
    partial class AssignResourcePage : 
        global::Microsoft.UI.Xaml.Controls.Page, 
        global::Microsoft.UI.Xaml.Markup.IComponentConnector
    {

        /// <summary>
        /// Connect()
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.UI.Xaml.Markup.Compiler"," 1.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void Connect(int connectionId, object target)
        {
            switch(connectionId)
            {
            case 2: // Views\AssignResourcePage.xaml line 11
                {
                    this.ProjectID_ComboBox = global::WinRT.CastExtensions.As<global::Microsoft.UI.Xaml.Controls.ComboBox>(target);
                    ((global::Microsoft.UI.Xaml.Controls.ComboBox)this.ProjectID_ComboBox).SelectionChanged += this.ProjectID_ComboBox_SelectionChanged;
                }
                break;
            case 3: // Views\AssignResourcePage.xaml line 13
                {
                    this.TaskID_ComboBox = global::WinRT.CastExtensions.As<global::Microsoft.UI.Xaml.Controls.ComboBox>(target);
                }
                break;
            case 4: // Views\AssignResourcePage.xaml line 16
                {
                    this.ResourceName_TextBox = global::WinRT.CastExtensions.As<global::Microsoft.UI.Xaml.Controls.TextBox>(target);
                }
                break;
            case 5: // Views\AssignResourcePage.xaml line 18
                {
                    this.ResourceType_ComboBox = global::WinRT.CastExtensions.As<global::Microsoft.UI.Xaml.Controls.ComboBox>(target);
                }
                break;
            case 6: // Views\AssignResourcePage.xaml line 21
                {
                    this.Cost_TextBox = global::WinRT.CastExtensions.As<global::Microsoft.UI.Xaml.Controls.TextBox>(target);
                }
                break;
            case 7: // Views\AssignResourcePage.xaml line 23
                {
                    this.AssignResource_Button = global::WinRT.CastExtensions.As<global::Microsoft.UI.Xaml.Controls.Button>(target);
                    ((global::Microsoft.UI.Xaml.Controls.Button)this.AssignResource_Button).Click += this.AssignResource_ButtonClick;
                }
                break;
            default:
                break;
            }
            this._contentLoaded = true;
        }

        /// <summary>
        /// GetBindingConnector(int connectionId, object target)
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.UI.Xaml.Markup.Compiler"," 1.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public global::Microsoft.UI.Xaml.Markup.IComponentConnector GetBindingConnector(int connectionId, object target)
        {
            global::Microsoft.UI.Xaml.Markup.IComponentConnector returnValue = null;
            return returnValue;
        }
    }
}

