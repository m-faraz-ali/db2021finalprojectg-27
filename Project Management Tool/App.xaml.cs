﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.UI.Xaml;

using Project_Management_Tool.Activation;
using Project_Management_Tool.Contracts.Services;
using Project_Management_Tool.Core.Contracts.Services;
using Project_Management_Tool.Core.Services;
using Project_Management_Tool.Helpers;
using Project_Management_Tool.Models;
using Project_Management_Tool.Services;
using Project_Management_Tool.ViewModels;
using Project_Management_Tool.Views;

namespace Project_Management_Tool;

// To learn more about WinUI 3, see https://docs.microsoft.com/windows/apps/winui/winui3/.
public partial class App : Application
{
    // The .NET Generic Host provides dependency injection, configuration, logging, and other services.
    // https://docs.microsoft.com/dotnet/core/extensions/generic-host
    // https://docs.microsoft.com/dotnet/core/extensions/dependency-injection
    // https://docs.microsoft.com/dotnet/core/extensions/configuration
    // https://docs.microsoft.com/dotnet/core/extensions/logging
    public IHost Host
    {
        get;
    }

    public static T GetService<T>()
        where T : class
    {
        if ((App.Current as App)!.Host.Services.GetService(typeof(T)) is not T service)
        {
            throw new ArgumentException($"{typeof(T)} needs to be registered in ConfigureServices within App.xaml.cs.");
        }

        return service;
    }

    public static WindowEx MainWindow { get; } = new MainWindow();

    public App()
    {
        InitializeComponent();

        Host = Microsoft.Extensions.Hosting.Host.
        CreateDefaultBuilder().
        UseContentRoot(AppContext.BaseDirectory).
        ConfigureServices((context, services) =>
        {
            // Default Activation Handler
            services.AddTransient<ActivationHandler<LaunchActivatedEventArgs>, DefaultActivationHandler>();

            // Other Activation Handlers

            // Services
            services.AddSingleton<ILocalSettingsService, LocalSettingsService>();
            services.AddSingleton<IThemeSelectorService, ThemeSelectorService>();
            services.AddTransient<INavigationViewService, NavigationViewService>();

            services.AddSingleton<IActivationService, ActivationService>();
            services.AddSingleton<IPageService, PageService>();
            services.AddSingleton<INavigationService, NavigationService>();

            // Core Services
            services.AddSingleton<IFileService, FileService>();

            // Views and ViewModels
            services.AddTransient<AttendanceHistoryViewModel>();
            services.AddTransient<AttendanceHistoryPage>();
            services.AddTransient<ShellPMViewModel>();
            services.AddTransient<ShellPMPage>();
            services.AddTransient<ViewTasksViewModel>();
            services.AddTransient<ViewTasksPage>();
            services.AddTransient<ViewResourcesViewModel>();
            services.AddTransient<ViewResourcesPage>();
            services.AddTransient<ViewProjectsViewModel>();
            services.AddTransient<ViewProjectsPage>();
            services.AddTransient<ViewMembersViewModel>();
            services.AddTransient<ViewMembersPage>();
            services.AddTransient<UpdateTaskViewModel>();
            services.AddTransient<UpdateTaskPage>();
            services.AddTransient<UpdateResourceViewModel>();
            services.AddTransient<UpdateResourcePage>();
            services.AddTransient<UpdateProjectViewModel>();
            services.AddTransient<UpdateProjectPage>();
            services.AddTransient<AssignResourceViewModel>();
            services.AddTransient<AssignResourcePage>();
            services.AddTransient<AddTaskViewModel>();
            services.AddTransient<AddTaskPage>();
            services.AddTransient<AddProjectsViewModel>();
            services.AddTransient<AddProjectsPage>();
            services.AddTransient<AddMembersViewModel>();
            services.AddTransient<AddMembersPage>();

            services.AddTransient<ShellHRViewModel>();
            services.AddTransient<ShellHRPage>();
            services.AddTransient<ViewPersonalHRInformationViewModel>();
            services.AddTransient<ViewPersonalHRInformationPage>();
            services.AddTransient<MarkAttendanceViewModel>();
            services.AddTransient<MarkAttendancePage>();
            services.AddTransient<ManageLeaveRequestViewModel>();
            services.AddTransient<ManageLeaveRequestPage>();

            services.AddTransient<LoginViewModel>();
            services.AddTransient<LoginPage>();
            services.AddTransient<UpdateProjectManagerViewModel>();
            services.AddTransient<UpdateProjectManagerPage>();
            services.AddTransient<ViewProjectManagersViewModel>();
            services.AddTransient<ViewProjectManagersPage>();
            services.AddTransient<AddProjectManagerViewModel>();
            services.AddTransient<AddProjectManagerPage>();
            services.AddTransient<ViewHRManagersViewModel>();
            services.AddTransient<ViewHRManagersPage>();
            services.AddTransient<UpdateHRManagerViewModel>();
            services.AddTransient<UpdateHRManagerPage>();
            services.AddTransient<ViewHRManagersViewModel>();

            services.AddTransient<AddHRManagerViewModel>();
            services.AddTransient<AddHRManagerPage>();
            services.AddTransient<UpdateEmployeeViewModel>();
            services.AddTransient<UpdateEmployeePage>();
            services.AddTransient<ViewEmployeesViewModel>();
            services.AddTransient<ViewEmployeesPage>();
            services.AddTransient<AddEmployeeViewModel>();
            services.AddTransient<AddEmployeePage>();
            services.AddTransient<SettingsViewModel>();
            services.AddTransient<SettingsPage>();

            services.AddTransient<ShellPage>();
            services.AddTransient<ShellViewModel>();


            
            services.AddTransient<EditLeaveViewModel>();
            services.AddTransient<EditLeavePage>();
            
            services.AddTransient<ViewTaskViewModel>();
            services.AddTransient<ViewTaskPage>();
            
            services.AddTransient<ViewLeaveViewModel>();
            services.AddTransient<ViewLeavePage>();
            
            services.AddTransient<AddLeaveViewModel>();
            services.AddTransient<AddLeavePage>();
            
            services.AddTransient<ViewInformationViewModel>();
            services.AddTransient<ViewInformationPage>();
            
            services.AddTransient<ShellEmpViewModel>();
            services.AddTransient<ShellEmpPage>();


            // Configuration
            services.Configure<LocalSettingsOptions>(context.Configuration.GetSection(nameof(LocalSettingsOptions)));
        }).
        Build();

        UnhandledException += App_UnhandledException;
    }

    private void App_UnhandledException(object sender, Microsoft.UI.Xaml.UnhandledExceptionEventArgs e)
    {
        // TODO: Log and handle exceptions as appropriate.
        // https://docs.microsoft.com/windows/windows-app-sdk/api/winrt/microsoft.ui.xaml.application.unhandledexception.
    }

    protected async override void OnLaunched(LaunchActivatedEventArgs args)
    {
        base.OnLaunched(args);

        await App.GetService<IActivationService>().ActivateAsync(args);
    }
}
