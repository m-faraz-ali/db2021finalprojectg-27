﻿namespace Project_Management_Tool.Behaviors;

public enum NavigationViewHeaderMode
{
    Always,
    Never,
    Minimal
}
